### 18.0.0 Update to Angular 18
* b41aa72 -- [CI/CD] Update packages.json version based on GitLab tag.
*   69acf80 -- Merge branch 'develop' into 'master'
|\  
| *   f5e5c63 -- Merge branch '73-update-to-angular-18' into 'develop'
| |\  
| | * 72b0713 -- Remove module option
| | * 6c8f929 -- Update pipeline for Angular 18
| | * 55ecbe1 -- Prettier fix
| | * 3862dd5 -- Update VERSION.txt
| | * 97e9d22 -- Migrate angular test
| | * 0772868 -- Change topology-model version
| | * 1b58baf -- Fix pipeline
| | * c1a8f08 -- Update to angular 18
| |/  
|/|   
* | 199de13 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* | 059a010 -- [CI/CD] Update packages.json version based on GitLab tag.
* | b30eee3 -- Merge branch 'develop' into 'master'
|\| 
| * 000f62c -- Merge branch 'fix-graph-scaling' into 'develop'
|/| 
| * 1fdf0be -- Fix scaling issues
| * a565599 -- Add missing node alt
|/  
* c55e43f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4f3a613 -- [CI/CD] Update packages.json version based on GitLab tag.
*   435b276 -- Merge branch 'update-sentinel-auth-version' into 'master'
|\  
| * 2f7e5db -- Update sentinel auth version
|/  
* bf2e57f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 103d591 -- [CI/CD] Update packages.json version based on GitLab tag.
*   78b365c -- Merge branch '72-update-to-angular-16' into 'master'
|\  
| * b2f5354 -- Update environment to accept keycloak
| * 312162f -- Update to Angular 16
|/  
* 2e0b14d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5ea02c2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   36b632a -- Merge branch '71-update-to-angular-15' into 'master'
|\  
| * 3cb19c5 -- Update additional packages
| * 2631070 -- Update to Angular 15
|/  
* 56d36b3 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9a24f23 -- [CI/CD] Update packages.json version based on GitLab tag.
*   cc8a117 -- Merge branch 'topology-legend' into 'master'
|\  
| * b847215 -- Update topology display
|/  
* 8f12f84 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4f021bd -- [CI/CD] Update packages.json version based on GitLab tag.
*   1a05ae1 -- Merge branch '71-fix-docker-icon-and-legend' into 'master'
|\  
| * be8b0d0 -- Fix docker icon and legend
|/  
* 5539f53 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4b78fd9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ff9fc40 -- Merge branch '70-add-support-for-docker-containers-in-topology' into 'master'
|\  
| * 19559f5 -- Resolve "Add support for Docker containers in topology"
|/  
* a92f56a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b2049f3 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e20edb7 -- Merge branch '69-fix-the-number-of-hidden-subnodes' into 'master'
|\  
| * 8c3e11e -- Resolve "Fix the number of hidden subnodes"
|/  
* 548a15c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cd2bd81 -- [CI/CD] Update packages.json version based on GitLab tag.
*   cd8710f -- Merge branch 'fix-guacamole' into 'master'
|\  
| * 352bfa7 -- Fix guacamole uuid
|/  
* f7f78b6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cd7a984 -- [CI/CD] Update packages.json version based on GitLab tag.
*   155455d -- Merge branch '68-change-sandbox-service-endpoints-that-use-number-data-type-to-string-for-sandbox-uuids' into 'master'
|\  
| * a737470 -- Resolve "Change sandbox service endpoints that use number data type to string for sandbox uuids"
|/  
* 762bc2a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cd52622 -- [CI/CD] Update packages.json version based on GitLab tag.
*   5a03e5b -- Merge branch '67-disable-spice-console-when-it-is-not-ready' into 'master'
|\  
| * bce7dc0 -- Resolve "Disable spice console when it is not ready"
|/  
*   2ac86af -- Merge branch '66-rework-drop-down-menu-in-topology' into 'master'
|\  
| * ce949cb -- Resolve "Rework drop down menu in topology"
|/  
* f10bc7b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 45780fd -- [CI/CD] Update packages.json version based on GitLab tag.
*   0d70187 -- Merge branch '65-rename-from-kypo2-to-kypo' into 'master'
|\  
| * 8e8d954 -- Resolve "Rename from kypo2 to kypo"
|/  
* d4b6392 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2276695 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ba17f3c -- Merge branch '64-fix-imports' into 'master'
|\  
| * 6d25f0e -- Resolve "Fix imports"
|/  
* 347f05e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 130df10 -- [CI/CD] Update packages.json version based on GitLab tag.
*   89e4c19 -- Merge branch '63-update-to-angular-14' into 'master'
|\  
| * 51310b6 -- Resolve "Update to Angular 14"
|/  
*   ac8272f -- Merge branch '62-fix-context-menu-clipping' into 'master'
|\  
| * c5ae6b4 -- Resolve "Fix context menu clipping"
|/  
* aedd30f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* ae7bb99 -- [CI/CD] Update packages.json version based on GitLab tag.
*   452a279 -- Merge branch 'fix-context-menu' into 'master'
|\  
| * 665d6ac -- Fix business logic of context menu
|/  
* 3cc2eec -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 16ecabb -- [CI/CD] Update packages.json version based on GitLab tag.
*   723cfad -- Merge branch '61-review-open-console-options-and-remove-resume-option' into 'master'
|\  
| * eec6e04 -- Removed Resume option from node menu. Renamed open console options.
|/  
* 16aa303 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f6e87de -- [CI/CD] Update packages.json version based on GitLab tag.
*   3e0770d -- Merge branch '60-update-to-angular-13' into 'master'
|\  
| * 08e1839 -- Resolve "Update to Angular 13"
|/  
*   9a43908 -- Merge branch '59-display-the-topology-based-on-the-sandbox-definition-id' into 'master'
|\  
| * 391a95f -- Resolve "Display the topology based on the sandbox definition ID"
|/  
* 4d6b41a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fe31cfd -- [CI/CD] Update packages.json version based on GitLab tag.
*   3faed73 -- Merge branch '58-bump-version' into 'master'
|\  
| * 292abe6 -- Bump version
|/  
*   8410e87 -- Merge branch '57-fix-auto-topology-expansion-with-small-number-of-nodes' into 'master'
|\  
| * 59e0050 -- Move topology expansion decision to visuals
|/  
* b1c971c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* edf8c00 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7773b1c -- Merge branch '56-bump-version-of-d3' into 'master'
|\  
| * e5e7fb2 -- Resolve "Bump version of d3"
|/  
* ba84d32 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 52c67ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   e3a5ddf -- Merge branch '55-add-attributes-gui_access-and-os_type-to-routerdto' into 'master'
|\  
| * 57823ce -- Add missing attributes. Modified mapper accordingly.
|/  
* 37d0f58 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b31153f -- [CI/CD] Update packages.json version based on GitLab tag.
*   ecbd2f5 -- Merge branch '53-dispaly-open-cli-and-open-gui-option-based-on-the-os-type-and-guiaccess-attribute-of-the-host' into 'master'
|\  
| * bd8032c -- Resolve "Dispaly Open CLI and Open GUI option based on the OS type and guiAccess attribute of the host"
|/  
*   d683ace -- Merge branch '54-add-console-preload' into 'master'
|\  
| * 69439f1 -- Resolve "Add console preload"
|/  
*   b740a39 -- Merge branch '51-switching-the-host-console-access-to-a-single-phase' into 'master'
|\  
| * 3d4a8f1 -- Remove loading of spice console
|/  
* 9b52172 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 340fa12 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e42f178 -- Merge branch '50-add-simple-legend-for-topology-nodes' into 'master'
|\  
| * 344b252 -- Resolve "Add simple legend for topology nodes"
|/  
*   cf7e3b1 -- Merge branch '52-add-license-file' into 'master'
|\  
| * 2fd2ad6 -- Add license file
|/  
*   1e4a62f -- Merge branch '49-update-the-look-of-topology-nodes' into 'master'
|\  
| * 00fad21 -- Update nodes visibility and icons
|/  
* a9aa75b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 73354b2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   34da99c -- Merge branch '48-add-option-to-open-console-using-apache-guacamole' into 'master'
|\  
| * 916ef3a -- Resolve "Add option to open console using apache guacamole"
|/  
* b8b6b33 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 70fd3a9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8a59576 -- Merge branch '47-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * b6a4002 -- Update gitlab CI
|/  
* a291334 -- Update project package.json version based on GitLab tag. Done by CI
*   65ea149 -- Merge branch '46-update-to-angular-12' into 'master'
|\  
| * cbe2590 -- Resolve "Update to Angular 12"
|/  
* 9e6fcf5 -- Update project package.json version based on GitLab tag. Done by CI
*   4654845 -- Merge branch '45-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0aeb4a5 -- Update peerDependencies and dependencies
|/  
*   ab1fdb6 -- Merge branch '44-update-to-angular-11' into 'master'
|\  
| * 90fe8a8 -- Resolve "Update to Angular 11"
|/  
*   2f94029 -- Merge branch '43-recreate-package-lock-for-new-package-registry-muni-kypo-crp-frontend-angular-models-kypo' into 'master'
|\  
| * b8c9c07 -- reacreate package lock
|/  
*   6db319a -- Merge branch '42-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 7fe3c8b -- Resolve "Migrate from tslint to eslint"
|/  
* af94615 -- Update project package.json version based on GitLab tag. Done by CI
*   105c759 -- Merge branch '41-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * eebd0de -- rename package scope and update dependencies
|/  
* 5f4642b -- Update project package.json version based on GitLab tag. Done by CI
*   6d4d5ea -- Merge branch '40-update-dependencies-to-new-format' into 'master'
|\  
| * a97e66c -- Resolve "Update dependencies to new format"
|/  
* 3fe51b8 -- Update project package.json version based on GitLab tag. Done by CI
*   670bd06 -- Merge branch '39-rename-package-to-kypo-topology-graph' into 'master'
|\  
| * f22bbe7 -- Resolve "Rename package to @kypo/topology-graph"
|/  
*   973f029 -- Merge branch '38-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * fa1b651 -- Remove personal info from README
|/  
*   aae734f -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 41bbf78 -- Resolve "Use Cypress image in CI"
|/  
* ae4f343 -- Update project package.json version based on GitLab tag. Done by CI
*   19a56f6 -- Merge branch '36-update-to-angualar-10' into 'master'
|\  
| * 4d6a53f -- Resolve "Update to Angular 10"
|/  
* bb653af -- Update project package.json version based on GitLab tag. Done by CI
### 16.0.2 Improve the appearance of the topology and the legend, fix scaling issues.
* 059a010 -- [CI/CD] Update packages.json version based on GitLab tag.
*   b30eee3 -- Merge branch 'develop' into 'master'
|\  
| * 000f62c -- Merge branch 'fix-graph-scaling' into 'develop'
|/| 
| * 1fdf0be -- Fix scaling issues
| * a565599 -- Add missing node alt
|/  
* c55e43f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4f3a613 -- [CI/CD] Update packages.json version based on GitLab tag.
*   435b276 -- Merge branch 'update-sentinel-auth-version' into 'master'
|\  
| * 2f7e5db -- Update sentinel auth version
|/  
* bf2e57f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 103d591 -- [CI/CD] Update packages.json version based on GitLab tag.
*   78b365c -- Merge branch '72-update-to-angular-16' into 'master'
|\  
| * b2f5354 -- Update environment to accept keycloak
| * 312162f -- Update to Angular 16
|/  
* 2e0b14d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5ea02c2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   36b632a -- Merge branch '71-update-to-angular-15' into 'master'
|\  
| * 3cb19c5 -- Update additional packages
| * 2631070 -- Update to Angular 15
|/  
* 56d36b3 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9a24f23 -- [CI/CD] Update packages.json version based on GitLab tag.
*   cc8a117 -- Merge branch 'topology-legend' into 'master'
|\  
| * b847215 -- Update topology display
|/  
* 8f12f84 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4f021bd -- [CI/CD] Update packages.json version based on GitLab tag.
*   1a05ae1 -- Merge branch '71-fix-docker-icon-and-legend' into 'master'
|\  
| * be8b0d0 -- Fix docker icon and legend
|/  
* 5539f53 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4b78fd9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ff9fc40 -- Merge branch '70-add-support-for-docker-containers-in-topology' into 'master'
|\  
| * 19559f5 -- Resolve "Add support for Docker containers in topology"
|/  
* a92f56a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b2049f3 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e20edb7 -- Merge branch '69-fix-the-number-of-hidden-subnodes' into 'master'
|\  
| * 8c3e11e -- Resolve "Fix the number of hidden subnodes"
|/  
* 548a15c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cd2bd81 -- [CI/CD] Update packages.json version based on GitLab tag.
*   cd8710f -- Merge branch 'fix-guacamole' into 'master'
|\  
| * 352bfa7 -- Fix guacamole uuid
|/  
* f7f78b6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cd7a984 -- [CI/CD] Update packages.json version based on GitLab tag.
*   155455d -- Merge branch '68-change-sandbox-service-endpoints-that-use-number-data-type-to-string-for-sandbox-uuids' into 'master'
|\  
| * a737470 -- Resolve "Change sandbox service endpoints that use number data type to string for sandbox uuids"
|/  
* 762bc2a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cd52622 -- [CI/CD] Update packages.json version based on GitLab tag.
*   5a03e5b -- Merge branch '67-disable-spice-console-when-it-is-not-ready' into 'master'
|\  
| * bce7dc0 -- Resolve "Disable spice console when it is not ready"
|/  
*   2ac86af -- Merge branch '66-rework-drop-down-menu-in-topology' into 'master'
|\  
| * ce949cb -- Resolve "Rework drop down menu in topology"
|/  
* f10bc7b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 45780fd -- [CI/CD] Update packages.json version based on GitLab tag.
*   0d70187 -- Merge branch '65-rename-from-kypo2-to-kypo' into 'master'
|\  
| * 8e8d954 -- Resolve "Rename from kypo2 to kypo"
|/  
* d4b6392 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2276695 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ba17f3c -- Merge branch '64-fix-imports' into 'master'
|\  
| * 6d25f0e -- Resolve "Fix imports"
|/  
* 347f05e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 130df10 -- [CI/CD] Update packages.json version based on GitLab tag.
*   89e4c19 -- Merge branch '63-update-to-angular-14' into 'master'
|\  
| * 51310b6 -- Resolve "Update to Angular 14"
|/  
*   ac8272f -- Merge branch '62-fix-context-menu-clipping' into 'master'
|\  
| * c5ae6b4 -- Resolve "Fix context menu clipping"
|/  
* aedd30f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* ae7bb99 -- [CI/CD] Update packages.json version based on GitLab tag.
*   452a279 -- Merge branch 'fix-context-menu' into 'master'
|\  
| * 665d6ac -- Fix business logic of context menu
|/  
* 3cc2eec -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 16ecabb -- [CI/CD] Update packages.json version based on GitLab tag.
*   723cfad -- Merge branch '61-review-open-console-options-and-remove-resume-option' into 'master'
|\  
| * eec6e04 -- Removed Resume option from node menu. Renamed open console options.
|/  
* 16aa303 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f6e87de -- [CI/CD] Update packages.json version based on GitLab tag.
*   3e0770d -- Merge branch '60-update-to-angular-13' into 'master'
|\  
| * 08e1839 -- Resolve "Update to Angular 13"
|/  
*   9a43908 -- Merge branch '59-display-the-topology-based-on-the-sandbox-definition-id' into 'master'
|\  
| * 391a95f -- Resolve "Display the topology based on the sandbox definition ID"
|/  
* 4d6b41a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fe31cfd -- [CI/CD] Update packages.json version based on GitLab tag.
*   3faed73 -- Merge branch '58-bump-version' into 'master'
|\  
| * 292abe6 -- Bump version
|/  
*   8410e87 -- Merge branch '57-fix-auto-topology-expansion-with-small-number-of-nodes' into 'master'
|\  
| * 59e0050 -- Move topology expansion decision to visuals
|/  
* b1c971c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* edf8c00 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7773b1c -- Merge branch '56-bump-version-of-d3' into 'master'
|\  
| * e5e7fb2 -- Resolve "Bump version of d3"
|/  
* ba84d32 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 52c67ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   e3a5ddf -- Merge branch '55-add-attributes-gui_access-and-os_type-to-routerdto' into 'master'
|\  
| * 57823ce -- Add missing attributes. Modified mapper accordingly.
|/  
* 37d0f58 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b31153f -- [CI/CD] Update packages.json version based on GitLab tag.
*   ecbd2f5 -- Merge branch '53-dispaly-open-cli-and-open-gui-option-based-on-the-os-type-and-guiaccess-attribute-of-the-host' into 'master'
|\  
| * bd8032c -- Resolve "Dispaly Open CLI and Open GUI option based on the OS type and guiAccess attribute of the host"
|/  
*   d683ace -- Merge branch '54-add-console-preload' into 'master'
|\  
| * 69439f1 -- Resolve "Add console preload"
|/  
*   b740a39 -- Merge branch '51-switching-the-host-console-access-to-a-single-phase' into 'master'
|\  
| * 3d4a8f1 -- Remove loading of spice console
|/  
* 9b52172 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 340fa12 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e42f178 -- Merge branch '50-add-simple-legend-for-topology-nodes' into 'master'
|\  
| * 344b252 -- Resolve "Add simple legend for topology nodes"
|/  
*   cf7e3b1 -- Merge branch '52-add-license-file' into 'master'
|\  
| * 2fd2ad6 -- Add license file
|/  
*   1e4a62f -- Merge branch '49-update-the-look-of-topology-nodes' into 'master'
|\  
| * 00fad21 -- Update nodes visibility and icons
|/  
* a9aa75b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 73354b2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   34da99c -- Merge branch '48-add-option-to-open-console-using-apache-guacamole' into 'master'
|\  
| * 916ef3a -- Resolve "Add option to open console using apache guacamole"
|/  
* b8b6b33 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 70fd3a9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8a59576 -- Merge branch '47-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * b6a4002 -- Update gitlab CI
|/  
* a291334 -- Update project package.json version based on GitLab tag. Done by CI
*   65ea149 -- Merge branch '46-update-to-angular-12' into 'master'
|\  
| * cbe2590 -- Resolve "Update to Angular 12"
|/  
* 9e6fcf5 -- Update project package.json version based on GitLab tag. Done by CI
*   4654845 -- Merge branch '45-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0aeb4a5 -- Update peerDependencies and dependencies
|/  
*   ab1fdb6 -- Merge branch '44-update-to-angular-11' into 'master'
|\  
| * 90fe8a8 -- Resolve "Update to Angular 11"
|/  
*   2f94029 -- Merge branch '43-recreate-package-lock-for-new-package-registry-muni-kypo-crp-frontend-angular-models-kypo' into 'master'
|\  
| * b8c9c07 -- reacreate package lock
|/  
*   6db319a -- Merge branch '42-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 7fe3c8b -- Resolve "Migrate from tslint to eslint"
|/  
* af94615 -- Update project package.json version based on GitLab tag. Done by CI
*   105c759 -- Merge branch '41-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * eebd0de -- rename package scope and update dependencies
|/  
* 5f4642b -- Update project package.json version based on GitLab tag. Done by CI
*   6d4d5ea -- Merge branch '40-update-dependencies-to-new-format' into 'master'
|\  
| * a97e66c -- Resolve "Update dependencies to new format"
|/  
* 3fe51b8 -- Update project package.json version based on GitLab tag. Done by CI
*   670bd06 -- Merge branch '39-rename-package-to-kypo-topology-graph' into 'master'
|\  
| * f22bbe7 -- Resolve "Rename package to @kypo/topology-graph"
|/  
*   973f029 -- Merge branch '38-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * fa1b651 -- Remove personal info from README
|/  
*   aae734f -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 41bbf78 -- Resolve "Use Cypress image in CI"
|/  
* ae4f343 -- Update project package.json version based on GitLab tag. Done by CI
*   19a56f6 -- Merge branch '36-update-to-angualar-10' into 'master'
|\  
| * 4d6a53f -- Resolve "Update to Angular 10"
|/  
* bb653af -- Update project package.json version based on GitLab tag. Done by CI
### 16.0.1 Update sentinel auth version.
* 4f3a613 -- [CI/CD] Update packages.json version based on GitLab tag.
*   435b276 -- Merge branch 'update-sentinel-auth-version' into 'master'
|\  
| * 2f7e5db -- Update sentinel auth version
|/  
* bf2e57f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 103d591 -- [CI/CD] Update packages.json version based on GitLab tag.
*   78b365c -- Merge branch '72-update-to-angular-16' into 'master'
|\  
| * b2f5354 -- Update environment to accept keycloak
| * 312162f -- Update to Angular 16
|/  
* 2e0b14d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5ea02c2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   36b632a -- Merge branch '71-update-to-angular-15' into 'master'
|\  
| * 3cb19c5 -- Update additional packages
| * 2631070 -- Update to Angular 15
|/  
* 56d36b3 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9a24f23 -- [CI/CD] Update packages.json version based on GitLab tag.
*   cc8a117 -- Merge branch 'topology-legend' into 'master'
|\  
| * b847215 -- Update topology display
|/  
* 8f12f84 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4f021bd -- [CI/CD] Update packages.json version based on GitLab tag.
*   1a05ae1 -- Merge branch '71-fix-docker-icon-and-legend' into 'master'
|\  
| * be8b0d0 -- Fix docker icon and legend
|/  
* 5539f53 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4b78fd9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ff9fc40 -- Merge branch '70-add-support-for-docker-containers-in-topology' into 'master'
|\  
| * 19559f5 -- Resolve "Add support for Docker containers in topology"
|/  
* a92f56a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b2049f3 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e20edb7 -- Merge branch '69-fix-the-number-of-hidden-subnodes' into 'master'
|\  
| * 8c3e11e -- Resolve "Fix the number of hidden subnodes"
|/  
* 548a15c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cd2bd81 -- [CI/CD] Update packages.json version based on GitLab tag.
*   cd8710f -- Merge branch 'fix-guacamole' into 'master'
|\  
| * 352bfa7 -- Fix guacamole uuid
|/  
* f7f78b6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cd7a984 -- [CI/CD] Update packages.json version based on GitLab tag.
*   155455d -- Merge branch '68-change-sandbox-service-endpoints-that-use-number-data-type-to-string-for-sandbox-uuids' into 'master'
|\  
| * a737470 -- Resolve "Change sandbox service endpoints that use number data type to string for sandbox uuids"
|/  
* 762bc2a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cd52622 -- [CI/CD] Update packages.json version based on GitLab tag.
*   5a03e5b -- Merge branch '67-disable-spice-console-when-it-is-not-ready' into 'master'
|\  
| * bce7dc0 -- Resolve "Disable spice console when it is not ready"
|/  
*   2ac86af -- Merge branch '66-rework-drop-down-menu-in-topology' into 'master'
|\  
| * ce949cb -- Resolve "Rework drop down menu in topology"
|/  
* f10bc7b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 45780fd -- [CI/CD] Update packages.json version based on GitLab tag.
*   0d70187 -- Merge branch '65-rename-from-kypo2-to-kypo' into 'master'
|\  
| * 8e8d954 -- Resolve "Rename from kypo2 to kypo"
|/  
* d4b6392 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2276695 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ba17f3c -- Merge branch '64-fix-imports' into 'master'
|\  
| * 6d25f0e -- Resolve "Fix imports"
|/  
* 347f05e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 130df10 -- [CI/CD] Update packages.json version based on GitLab tag.
*   89e4c19 -- Merge branch '63-update-to-angular-14' into 'master'
|\  
| * 51310b6 -- Resolve "Update to Angular 14"
|/  
*   ac8272f -- Merge branch '62-fix-context-menu-clipping' into 'master'
|\  
| * c5ae6b4 -- Resolve "Fix context menu clipping"
|/  
* aedd30f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* ae7bb99 -- [CI/CD] Update packages.json version based on GitLab tag.
*   452a279 -- Merge branch 'fix-context-menu' into 'master'
|\  
| * 665d6ac -- Fix business logic of context menu
|/  
* 3cc2eec -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 16ecabb -- [CI/CD] Update packages.json version based on GitLab tag.
*   723cfad -- Merge branch '61-review-open-console-options-and-remove-resume-option' into 'master'
|\  
| * eec6e04 -- Removed Resume option from node menu. Renamed open console options.
|/  
* 16aa303 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f6e87de -- [CI/CD] Update packages.json version based on GitLab tag.
*   3e0770d -- Merge branch '60-update-to-angular-13' into 'master'
|\  
| * 08e1839 -- Resolve "Update to Angular 13"
|/  
*   9a43908 -- Merge branch '59-display-the-topology-based-on-the-sandbox-definition-id' into 'master'
|\  
| * 391a95f -- Resolve "Display the topology based on the sandbox definition ID"
|/  
* 4d6b41a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fe31cfd -- [CI/CD] Update packages.json version based on GitLab tag.
*   3faed73 -- Merge branch '58-bump-version' into 'master'
|\  
| * 292abe6 -- Bump version
|/  
*   8410e87 -- Merge branch '57-fix-auto-topology-expansion-with-small-number-of-nodes' into 'master'
|\  
| * 59e0050 -- Move topology expansion decision to visuals
|/  
* b1c971c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* edf8c00 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7773b1c -- Merge branch '56-bump-version-of-d3' into 'master'
|\  
| * e5e7fb2 -- Resolve "Bump version of d3"
|/  
* ba84d32 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 52c67ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   e3a5ddf -- Merge branch '55-add-attributes-gui_access-and-os_type-to-routerdto' into 'master'
|\  
| * 57823ce -- Add missing attributes. Modified mapper accordingly.
|/  
* 37d0f58 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b31153f -- [CI/CD] Update packages.json version based on GitLab tag.
*   ecbd2f5 -- Merge branch '53-dispaly-open-cli-and-open-gui-option-based-on-the-os-type-and-guiaccess-attribute-of-the-host' into 'master'
|\  
| * bd8032c -- Resolve "Dispaly Open CLI and Open GUI option based on the OS type and guiAccess attribute of the host"
|/  
*   d683ace -- Merge branch '54-add-console-preload' into 'master'
|\  
| * 69439f1 -- Resolve "Add console preload"
|/  
*   b740a39 -- Merge branch '51-switching-the-host-console-access-to-a-single-phase' into 'master'
|\  
| * 3d4a8f1 -- Remove loading of spice console
|/  
* 9b52172 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 340fa12 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e42f178 -- Merge branch '50-add-simple-legend-for-topology-nodes' into 'master'
|\  
| * 344b252 -- Resolve "Add simple legend for topology nodes"
|/  
*   cf7e3b1 -- Merge branch '52-add-license-file' into 'master'
|\  
| * 2fd2ad6 -- Add license file
|/  
*   1e4a62f -- Merge branch '49-update-the-look-of-topology-nodes' into 'master'
|\  
| * 00fad21 -- Update nodes visibility and icons
|/  
* a9aa75b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 73354b2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   34da99c -- Merge branch '48-add-option-to-open-console-using-apache-guacamole' into 'master'
|\  
| * 916ef3a -- Resolve "Add option to open console using apache guacamole"
|/  
* b8b6b33 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 70fd3a9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8a59576 -- Merge branch '47-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * b6a4002 -- Update gitlab CI
|/  
* a291334 -- Update project package.json version based on GitLab tag. Done by CI
*   65ea149 -- Merge branch '46-update-to-angular-12' into 'master'
|\  
| * cbe2590 -- Resolve "Update to Angular 12"
|/  
* 9e6fcf5 -- Update project package.json version based on GitLab tag. Done by CI
*   4654845 -- Merge branch '45-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0aeb4a5 -- Update peerDependencies and dependencies
|/  
*   ab1fdb6 -- Merge branch '44-update-to-angular-11' into 'master'
|\  
| * 90fe8a8 -- Resolve "Update to Angular 11"
|/  
*   2f94029 -- Merge branch '43-recreate-package-lock-for-new-package-registry-muni-kypo-crp-frontend-angular-models-kypo' into 'master'
|\  
| * b8c9c07 -- reacreate package lock
|/  
*   6db319a -- Merge branch '42-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 7fe3c8b -- Resolve "Migrate from tslint to eslint"
|/  
* af94615 -- Update project package.json version based on GitLab tag. Done by CI
*   105c759 -- Merge branch '41-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * eebd0de -- rename package scope and update dependencies
|/  
* 5f4642b -- Update project package.json version based on GitLab tag. Done by CI
*   6d4d5ea -- Merge branch '40-update-dependencies-to-new-format' into 'master'
|\  
| * a97e66c -- Resolve "Update dependencies to new format"
|/  
* 3fe51b8 -- Update project package.json version based on GitLab tag. Done by CI
*   670bd06 -- Merge branch '39-rename-package-to-kypo-topology-graph' into 'master'
|\  
| * f22bbe7 -- Resolve "Rename package to @kypo/topology-graph"
|/  
*   973f029 -- Merge branch '38-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * fa1b651 -- Remove personal info from README
|/  
*   aae734f -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 41bbf78 -- Resolve "Use Cypress image in CI"
|/  
* ae4f343 -- Update project package.json version based on GitLab tag. Done by CI
*   19a56f6 -- Merge branch '36-update-to-angualar-10' into 'master'
|\  
| * 4d6a53f -- Resolve "Update to Angular 10"
|/  
* bb653af -- Update project package.json version based on GitLab tag. Done by CI
### 16.0.0 Update to Angular 16.
* 103d591 -- [CI/CD] Update packages.json version based on GitLab tag.
*   78b365c -- Merge branch '72-update-to-angular-16' into 'master'
|\  
| * b2f5354 -- Update environment to accept keycloak
| * 312162f -- Update to Angular 16
|/  
* 2e0b14d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5ea02c2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   36b632a -- Merge branch '71-update-to-angular-15' into 'master'
|\  
| * 3cb19c5 -- Update additional packages
| * 2631070 -- Update to Angular 15
|/  
* 56d36b3 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9a24f23 -- [CI/CD] Update packages.json version based on GitLab tag.
*   cc8a117 -- Merge branch 'topology-legend' into 'master'
|\  
| * b847215 -- Update topology display
|/  
* 8f12f84 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4f021bd -- [CI/CD] Update packages.json version based on GitLab tag.
*   1a05ae1 -- Merge branch '71-fix-docker-icon-and-legend' into 'master'
|\  
| * be8b0d0 -- Fix docker icon and legend
|/  
* 5539f53 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4b78fd9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ff9fc40 -- Merge branch '70-add-support-for-docker-containers-in-topology' into 'master'
|\  
| * 19559f5 -- Resolve "Add support for Docker containers in topology"
|/  
* a92f56a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b2049f3 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e20edb7 -- Merge branch '69-fix-the-number-of-hidden-subnodes' into 'master'
|\  
| * 8c3e11e -- Resolve "Fix the number of hidden subnodes"
|/  
* 548a15c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cd2bd81 -- [CI/CD] Update packages.json version based on GitLab tag.
*   cd8710f -- Merge branch 'fix-guacamole' into 'master'
|\  
| * 352bfa7 -- Fix guacamole uuid
|/  
* f7f78b6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cd7a984 -- [CI/CD] Update packages.json version based on GitLab tag.
*   155455d -- Merge branch '68-change-sandbox-service-endpoints-that-use-number-data-type-to-string-for-sandbox-uuids' into 'master'
|\  
| * a737470 -- Resolve "Change sandbox service endpoints that use number data type to string for sandbox uuids"
|/  
* 762bc2a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cd52622 -- [CI/CD] Update packages.json version based on GitLab tag.
*   5a03e5b -- Merge branch '67-disable-spice-console-when-it-is-not-ready' into 'master'
|\  
| * bce7dc0 -- Resolve "Disable spice console when it is not ready"
|/  
*   2ac86af -- Merge branch '66-rework-drop-down-menu-in-topology' into 'master'
|\  
| * ce949cb -- Resolve "Rework drop down menu in topology"
|/  
* f10bc7b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 45780fd -- [CI/CD] Update packages.json version based on GitLab tag.
*   0d70187 -- Merge branch '65-rename-from-kypo2-to-kypo' into 'master'
|\  
| * 8e8d954 -- Resolve "Rename from kypo2 to kypo"
|/  
* d4b6392 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2276695 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ba17f3c -- Merge branch '64-fix-imports' into 'master'
|\  
| * 6d25f0e -- Resolve "Fix imports"
|/  
* 347f05e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 130df10 -- [CI/CD] Update packages.json version based on GitLab tag.
*   89e4c19 -- Merge branch '63-update-to-angular-14' into 'master'
|\  
| * 51310b6 -- Resolve "Update to Angular 14"
|/  
*   ac8272f -- Merge branch '62-fix-context-menu-clipping' into 'master'
|\  
| * c5ae6b4 -- Resolve "Fix context menu clipping"
|/  
* aedd30f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* ae7bb99 -- [CI/CD] Update packages.json version based on GitLab tag.
*   452a279 -- Merge branch 'fix-context-menu' into 'master'
|\  
| * 665d6ac -- Fix business logic of context menu
|/  
* 3cc2eec -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 16ecabb -- [CI/CD] Update packages.json version based on GitLab tag.
*   723cfad -- Merge branch '61-review-open-console-options-and-remove-resume-option' into 'master'
|\  
| * eec6e04 -- Removed Resume option from node menu. Renamed open console options.
|/  
* 16aa303 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f6e87de -- [CI/CD] Update packages.json version based on GitLab tag.
*   3e0770d -- Merge branch '60-update-to-angular-13' into 'master'
|\  
| * 08e1839 -- Resolve "Update to Angular 13"
|/  
*   9a43908 -- Merge branch '59-display-the-topology-based-on-the-sandbox-definition-id' into 'master'
|\  
| * 391a95f -- Resolve "Display the topology based on the sandbox definition ID"
|/  
* 4d6b41a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fe31cfd -- [CI/CD] Update packages.json version based on GitLab tag.
*   3faed73 -- Merge branch '58-bump-version' into 'master'
|\  
| * 292abe6 -- Bump version
|/  
*   8410e87 -- Merge branch '57-fix-auto-topology-expansion-with-small-number-of-nodes' into 'master'
|\  
| * 59e0050 -- Move topology expansion decision to visuals
|/  
* b1c971c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* edf8c00 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7773b1c -- Merge branch '56-bump-version-of-d3' into 'master'
|\  
| * e5e7fb2 -- Resolve "Bump version of d3"
|/  
* ba84d32 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 52c67ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   e3a5ddf -- Merge branch '55-add-attributes-gui_access-and-os_type-to-routerdto' into 'master'
|\  
| * 57823ce -- Add missing attributes. Modified mapper accordingly.
|/  
* 37d0f58 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b31153f -- [CI/CD] Update packages.json version based on GitLab tag.
*   ecbd2f5 -- Merge branch '53-dispaly-open-cli-and-open-gui-option-based-on-the-os-type-and-guiaccess-attribute-of-the-host' into 'master'
|\  
| * bd8032c -- Resolve "Dispaly Open CLI and Open GUI option based on the OS type and guiAccess attribute of the host"
|/  
*   d683ace -- Merge branch '54-add-console-preload' into 'master'
|\  
| * 69439f1 -- Resolve "Add console preload"
|/  
*   b740a39 -- Merge branch '51-switching-the-host-console-access-to-a-single-phase' into 'master'
|\  
| * 3d4a8f1 -- Remove loading of spice console
|/  
* 9b52172 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 340fa12 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e42f178 -- Merge branch '50-add-simple-legend-for-topology-nodes' into 'master'
|\  
| * 344b252 -- Resolve "Add simple legend for topology nodes"
|/  
*   cf7e3b1 -- Merge branch '52-add-license-file' into 'master'
|\  
| * 2fd2ad6 -- Add license file
|/  
*   1e4a62f -- Merge branch '49-update-the-look-of-topology-nodes' into 'master'
|\  
| * 00fad21 -- Update nodes visibility and icons
|/  
* a9aa75b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 73354b2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   34da99c -- Merge branch '48-add-option-to-open-console-using-apache-guacamole' into 'master'
|\  
| * 916ef3a -- Resolve "Add option to open console using apache guacamole"
|/  
* b8b6b33 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 70fd3a9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8a59576 -- Merge branch '47-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * b6a4002 -- Update gitlab CI
|/  
* a291334 -- Update project package.json version based on GitLab tag. Done by CI
*   65ea149 -- Merge branch '46-update-to-angular-12' into 'master'
|\  
| * cbe2590 -- Resolve "Update to Angular 12"
|/  
* 9e6fcf5 -- Update project package.json version based on GitLab tag. Done by CI
*   4654845 -- Merge branch '45-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0aeb4a5 -- Update peerDependencies and dependencies
|/  
*   ab1fdb6 -- Merge branch '44-update-to-angular-11' into 'master'
|\  
| * 90fe8a8 -- Resolve "Update to Angular 11"
|/  
*   2f94029 -- Merge branch '43-recreate-package-lock-for-new-package-registry-muni-kypo-crp-frontend-angular-models-kypo' into 'master'
|\  
| * b8c9c07 -- reacreate package lock
|/  
*   6db319a -- Merge branch '42-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 7fe3c8b -- Resolve "Migrate from tslint to eslint"
|/  
* af94615 -- Update project package.json version based on GitLab tag. Done by CI
*   105c759 -- Merge branch '41-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * eebd0de -- rename package scope and update dependencies
|/  
* 5f4642b -- Update project package.json version based on GitLab tag. Done by CI
*   6d4d5ea -- Merge branch '40-update-dependencies-to-new-format' into 'master'
|\  
| * a97e66c -- Resolve "Update dependencies to new format"
|/  
* 3fe51b8 -- Update project package.json version based on GitLab tag. Done by CI
*   670bd06 -- Merge branch '39-rename-package-to-kypo-topology-graph' into 'master'
|\  
| * f22bbe7 -- Resolve "Rename package to @kypo/topology-graph"
|/  
*   973f029 -- Merge branch '38-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * fa1b651 -- Remove personal info from README
|/  
*   aae734f -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 41bbf78 -- Resolve "Use Cypress image in CI"
|/  
* ae4f343 -- Update project package.json version based on GitLab tag. Done by CI
*   19a56f6 -- Merge branch '36-update-to-angualar-10' into 'master'
|\  
| * 4d6a53f -- Resolve "Update to Angular 10"
|/  
* bb653af -- Update project package.json version based on GitLab tag. Done by CI
### 15.0.0 Update to Angular 15
* 5ea02c2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   36b632a -- Merge branch '71-update-to-angular-15' into 'master'
|\  
| * 3cb19c5 -- Update additional packages
| * 2631070 -- Update to Angular 15
|/  
* 56d36b3 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9a24f23 -- [CI/CD] Update packages.json version based on GitLab tag.
*   cc8a117 -- Merge branch 'topology-legend' into 'master'
|\  
| * b847215 -- Update topology display
|/  
* 8f12f84 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4f021bd -- [CI/CD] Update packages.json version based on GitLab tag.
*   1a05ae1 -- Merge branch '71-fix-docker-icon-and-legend' into 'master'
|\  
| * be8b0d0 -- Fix docker icon and legend
|/  
* 5539f53 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4b78fd9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ff9fc40 -- Merge branch '70-add-support-for-docker-containers-in-topology' into 'master'
|\  
| * 19559f5 -- Resolve "Add support for Docker containers in topology"
|/  
* a92f56a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b2049f3 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e20edb7 -- Merge branch '69-fix-the-number-of-hidden-subnodes' into 'master'
|\  
| * 8c3e11e -- Resolve "Fix the number of hidden subnodes"
|/  
* 548a15c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cd2bd81 -- [CI/CD] Update packages.json version based on GitLab tag.
*   cd8710f -- Merge branch 'fix-guacamole' into 'master'
|\  
| * 352bfa7 -- Fix guacamole uuid
|/  
* f7f78b6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cd7a984 -- [CI/CD] Update packages.json version based on GitLab tag.
*   155455d -- Merge branch '68-change-sandbox-service-endpoints-that-use-number-data-type-to-string-for-sandbox-uuids' into 'master'
|\  
| * a737470 -- Resolve "Change sandbox service endpoints that use number data type to string for sandbox uuids"
|/  
* 762bc2a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cd52622 -- [CI/CD] Update packages.json version based on GitLab tag.
*   5a03e5b -- Merge branch '67-disable-spice-console-when-it-is-not-ready' into 'master'
|\  
| * bce7dc0 -- Resolve "Disable spice console when it is not ready"
|/  
*   2ac86af -- Merge branch '66-rework-drop-down-menu-in-topology' into 'master'
|\  
| * ce949cb -- Resolve "Rework drop down menu in topology"
|/  
* f10bc7b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 45780fd -- [CI/CD] Update packages.json version based on GitLab tag.
*   0d70187 -- Merge branch '65-rename-from-kypo2-to-kypo' into 'master'
|\  
| * 8e8d954 -- Resolve "Rename from kypo2 to kypo"
|/  
* d4b6392 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2276695 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ba17f3c -- Merge branch '64-fix-imports' into 'master'
|\  
| * 6d25f0e -- Resolve "Fix imports"
|/  
* 347f05e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 130df10 -- [CI/CD] Update packages.json version based on GitLab tag.
*   89e4c19 -- Merge branch '63-update-to-angular-14' into 'master'
|\  
| * 51310b6 -- Resolve "Update to Angular 14"
|/  
*   ac8272f -- Merge branch '62-fix-context-menu-clipping' into 'master'
|\  
| * c5ae6b4 -- Resolve "Fix context menu clipping"
|/  
* aedd30f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* ae7bb99 -- [CI/CD] Update packages.json version based on GitLab tag.
*   452a279 -- Merge branch 'fix-context-menu' into 'master'
|\  
| * 665d6ac -- Fix business logic of context menu
|/  
* 3cc2eec -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 16ecabb -- [CI/CD] Update packages.json version based on GitLab tag.
*   723cfad -- Merge branch '61-review-open-console-options-and-remove-resume-option' into 'master'
|\  
| * eec6e04 -- Removed Resume option from node menu. Renamed open console options.
|/  
* 16aa303 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f6e87de -- [CI/CD] Update packages.json version based on GitLab tag.
*   3e0770d -- Merge branch '60-update-to-angular-13' into 'master'
|\  
| * 08e1839 -- Resolve "Update to Angular 13"
|/  
*   9a43908 -- Merge branch '59-display-the-topology-based-on-the-sandbox-definition-id' into 'master'
|\  
| * 391a95f -- Resolve "Display the topology based on the sandbox definition ID"
|/  
* 4d6b41a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fe31cfd -- [CI/CD] Update packages.json version based on GitLab tag.
*   3faed73 -- Merge branch '58-bump-version' into 'master'
|\  
| * 292abe6 -- Bump version
|/  
*   8410e87 -- Merge branch '57-fix-auto-topology-expansion-with-small-number-of-nodes' into 'master'
|\  
| * 59e0050 -- Move topology expansion decision to visuals
|/  
* b1c971c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* edf8c00 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7773b1c -- Merge branch '56-bump-version-of-d3' into 'master'
|\  
| * e5e7fb2 -- Resolve "Bump version of d3"
|/  
* ba84d32 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 52c67ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   e3a5ddf -- Merge branch '55-add-attributes-gui_access-and-os_type-to-routerdto' into 'master'
|\  
| * 57823ce -- Add missing attributes. Modified mapper accordingly.
|/  
* 37d0f58 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b31153f -- [CI/CD] Update packages.json version based on GitLab tag.
*   ecbd2f5 -- Merge branch '53-dispaly-open-cli-and-open-gui-option-based-on-the-os-type-and-guiaccess-attribute-of-the-host' into 'master'
|\  
| * bd8032c -- Resolve "Dispaly Open CLI and Open GUI option based on the OS type and guiAccess attribute of the host"
|/  
*   d683ace -- Merge branch '54-add-console-preload' into 'master'
|\  
| * 69439f1 -- Resolve "Add console preload"
|/  
*   b740a39 -- Merge branch '51-switching-the-host-console-access-to-a-single-phase' into 'master'
|\  
| * 3d4a8f1 -- Remove loading of spice console
|/  
* 9b52172 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 340fa12 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e42f178 -- Merge branch '50-add-simple-legend-for-topology-nodes' into 'master'
|\  
| * 344b252 -- Resolve "Add simple legend for topology nodes"
|/  
*   cf7e3b1 -- Merge branch '52-add-license-file' into 'master'
|\  
| * 2fd2ad6 -- Add license file
|/  
*   1e4a62f -- Merge branch '49-update-the-look-of-topology-nodes' into 'master'
|\  
| * 00fad21 -- Update nodes visibility and icons
|/  
* a9aa75b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 73354b2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   34da99c -- Merge branch '48-add-option-to-open-console-using-apache-guacamole' into 'master'
|\  
| * 916ef3a -- Resolve "Add option to open console using apache guacamole"
|/  
* b8b6b33 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 70fd3a9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8a59576 -- Merge branch '47-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * b6a4002 -- Update gitlab CI
|/  
* a291334 -- Update project package.json version based on GitLab tag. Done by CI
*   65ea149 -- Merge branch '46-update-to-angular-12' into 'master'
|\  
| * cbe2590 -- Resolve "Update to Angular 12"
|/  
* 9e6fcf5 -- Update project package.json version based on GitLab tag. Done by CI
*   4654845 -- Merge branch '45-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0aeb4a5 -- Update peerDependencies and dependencies
|/  
*   ab1fdb6 -- Merge branch '44-update-to-angular-11' into 'master'
|\  
| * 90fe8a8 -- Resolve "Update to Angular 11"
|/  
*   2f94029 -- Merge branch '43-recreate-package-lock-for-new-package-registry-muni-kypo-crp-frontend-angular-models-kypo' into 'master'
|\  
| * b8c9c07 -- reacreate package lock
|/  
*   6db319a -- Merge branch '42-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 7fe3c8b -- Resolve "Migrate from tslint to eslint"
|/  
* af94615 -- Update project package.json version based on GitLab tag. Done by CI
*   105c759 -- Merge branch '41-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * eebd0de -- rename package scope and update dependencies
|/  
* 5f4642b -- Update project package.json version based on GitLab tag. Done by CI
*   6d4d5ea -- Merge branch '40-update-dependencies-to-new-format' into 'master'
|\  
| * a97e66c -- Resolve "Update dependencies to new format"
|/  
* 3fe51b8 -- Update project package.json version based on GitLab tag. Done by CI
*   670bd06 -- Merge branch '39-rename-package-to-kypo-topology-graph' into 'master'
|\  
| * f22bbe7 -- Resolve "Rename package to @kypo/topology-graph"
|/  
*   973f029 -- Merge branch '38-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * fa1b651 -- Remove personal info from README
|/  
*   aae734f -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 41bbf78 -- Resolve "Use Cypress image in CI"
|/  
* ae4f343 -- Update project package.json version based on GitLab tag. Done by CI
*   19a56f6 -- Merge branch '36-update-to-angualar-10' into 'master'
|\  
| * 4d6a53f -- Resolve "Update to Angular 10"
|/  
* bb653af -- Update project package.json version based on GitLab tag. Done by CI
### 14.3.4 Update topology legend display and cursor settings
* 9a24f23 -- [CI/CD] Update packages.json version based on GitLab tag.
*   cc8a117 -- Merge branch 'topology-legend' into 'master'
|\  
| * b847215 -- Update topology display
|/  
* 8f12f84 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4f021bd -- [CI/CD] Update packages.json version based on GitLab tag.
*   1a05ae1 -- Merge branch '71-fix-docker-icon-and-legend' into 'master'
|\  
| * be8b0d0 -- Fix docker icon and legend
|/  
* 5539f53 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4b78fd9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ff9fc40 -- Merge branch '70-add-support-for-docker-containers-in-topology' into 'master'
|\  
| * 19559f5 -- Resolve "Add support for Docker containers in topology"
|/  
* a92f56a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b2049f3 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e20edb7 -- Merge branch '69-fix-the-number-of-hidden-subnodes' into 'master'
|\  
| * 8c3e11e -- Resolve "Fix the number of hidden subnodes"
|/  
* 548a15c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cd2bd81 -- [CI/CD] Update packages.json version based on GitLab tag.
*   cd8710f -- Merge branch 'fix-guacamole' into 'master'
|\  
| * 352bfa7 -- Fix guacamole uuid
|/  
* f7f78b6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cd7a984 -- [CI/CD] Update packages.json version based on GitLab tag.
*   155455d -- Merge branch '68-change-sandbox-service-endpoints-that-use-number-data-type-to-string-for-sandbox-uuids' into 'master'
|\  
| * a737470 -- Resolve "Change sandbox service endpoints that use number data type to string for sandbox uuids"
|/  
* 762bc2a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cd52622 -- [CI/CD] Update packages.json version based on GitLab tag.
*   5a03e5b -- Merge branch '67-disable-spice-console-when-it-is-not-ready' into 'master'
|\  
| * bce7dc0 -- Resolve "Disable spice console when it is not ready"
|/  
*   2ac86af -- Merge branch '66-rework-drop-down-menu-in-topology' into 'master'
|\  
| * ce949cb -- Resolve "Rework drop down menu in topology"
|/  
* f10bc7b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 45780fd -- [CI/CD] Update packages.json version based on GitLab tag.
*   0d70187 -- Merge branch '65-rename-from-kypo2-to-kypo' into 'master'
|\  
| * 8e8d954 -- Resolve "Rename from kypo2 to kypo"
|/  
* d4b6392 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2276695 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ba17f3c -- Merge branch '64-fix-imports' into 'master'
|\  
| * 6d25f0e -- Resolve "Fix imports"
|/  
* 347f05e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 130df10 -- [CI/CD] Update packages.json version based on GitLab tag.
*   89e4c19 -- Merge branch '63-update-to-angular-14' into 'master'
|\  
| * 51310b6 -- Resolve "Update to Angular 14"
|/  
*   ac8272f -- Merge branch '62-fix-context-menu-clipping' into 'master'
|\  
| * c5ae6b4 -- Resolve "Fix context menu clipping"
|/  
* aedd30f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* ae7bb99 -- [CI/CD] Update packages.json version based on GitLab tag.
*   452a279 -- Merge branch 'fix-context-menu' into 'master'
|\  
| * 665d6ac -- Fix business logic of context menu
|/  
* 3cc2eec -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 16ecabb -- [CI/CD] Update packages.json version based on GitLab tag.
*   723cfad -- Merge branch '61-review-open-console-options-and-remove-resume-option' into 'master'
|\  
| * eec6e04 -- Removed Resume option from node menu. Renamed open console options.
|/  
* 16aa303 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f6e87de -- [CI/CD] Update packages.json version based on GitLab tag.
*   3e0770d -- Merge branch '60-update-to-angular-13' into 'master'
|\  
| * 08e1839 -- Resolve "Update to Angular 13"
|/  
*   9a43908 -- Merge branch '59-display-the-topology-based-on-the-sandbox-definition-id' into 'master'
|\  
| * 391a95f -- Resolve "Display the topology based on the sandbox definition ID"
|/  
* 4d6b41a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fe31cfd -- [CI/CD] Update packages.json version based on GitLab tag.
*   3faed73 -- Merge branch '58-bump-version' into 'master'
|\  
| * 292abe6 -- Bump version
|/  
*   8410e87 -- Merge branch '57-fix-auto-topology-expansion-with-small-number-of-nodes' into 'master'
|\  
| * 59e0050 -- Move topology expansion decision to visuals
|/  
* b1c971c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* edf8c00 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7773b1c -- Merge branch '56-bump-version-of-d3' into 'master'
|\  
| * e5e7fb2 -- Resolve "Bump version of d3"
|/  
* ba84d32 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 52c67ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   e3a5ddf -- Merge branch '55-add-attributes-gui_access-and-os_type-to-routerdto' into 'master'
|\  
| * 57823ce -- Add missing attributes. Modified mapper accordingly.
|/  
* 37d0f58 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b31153f -- [CI/CD] Update packages.json version based on GitLab tag.
*   ecbd2f5 -- Merge branch '53-dispaly-open-cli-and-open-gui-option-based-on-the-os-type-and-guiaccess-attribute-of-the-host' into 'master'
|\  
| * bd8032c -- Resolve "Dispaly Open CLI and Open GUI option based on the OS type and guiAccess attribute of the host"
|/  
*   d683ace -- Merge branch '54-add-console-preload' into 'master'
|\  
| * 69439f1 -- Resolve "Add console preload"
|/  
*   b740a39 -- Merge branch '51-switching-the-host-console-access-to-a-single-phase' into 'master'
|\  
| * 3d4a8f1 -- Remove loading of spice console
|/  
* 9b52172 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 340fa12 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e42f178 -- Merge branch '50-add-simple-legend-for-topology-nodes' into 'master'
|\  
| * 344b252 -- Resolve "Add simple legend for topology nodes"
|/  
*   cf7e3b1 -- Merge branch '52-add-license-file' into 'master'
|\  
| * 2fd2ad6 -- Add license file
|/  
*   1e4a62f -- Merge branch '49-update-the-look-of-topology-nodes' into 'master'
|\  
| * 00fad21 -- Update nodes visibility and icons
|/  
* a9aa75b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 73354b2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   34da99c -- Merge branch '48-add-option-to-open-console-using-apache-guacamole' into 'master'
|\  
| * 916ef3a -- Resolve "Add option to open console using apache guacamole"
|/  
* b8b6b33 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 70fd3a9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8a59576 -- Merge branch '47-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * b6a4002 -- Update gitlab CI
|/  
* a291334 -- Update project package.json version based on GitLab tag. Done by CI
*   65ea149 -- Merge branch '46-update-to-angular-12' into 'master'
|\  
| * cbe2590 -- Resolve "Update to Angular 12"
|/  
* 9e6fcf5 -- Update project package.json version based on GitLab tag. Done by CI
*   4654845 -- Merge branch '45-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0aeb4a5 -- Update peerDependencies and dependencies
|/  
*   ab1fdb6 -- Merge branch '44-update-to-angular-11' into 'master'
|\  
| * 90fe8a8 -- Resolve "Update to Angular 11"
|/  
*   2f94029 -- Merge branch '43-recreate-package-lock-for-new-package-registry-muni-kypo-crp-frontend-angular-models-kypo' into 'master'
|\  
| * b8c9c07 -- reacreate package lock
|/  
*   6db319a -- Merge branch '42-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 7fe3c8b -- Resolve "Migrate from tslint to eslint"
|/  
* af94615 -- Update project package.json version based on GitLab tag. Done by CI
*   105c759 -- Merge branch '41-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * eebd0de -- rename package scope and update dependencies
|/  
* 5f4642b -- Update project package.json version based on GitLab tag. Done by CI
*   6d4d5ea -- Merge branch '40-update-dependencies-to-new-format' into 'master'
|\  
| * a97e66c -- Resolve "Update dependencies to new format"
|/  
* 3fe51b8 -- Update project package.json version based on GitLab tag. Done by CI
*   670bd06 -- Merge branch '39-rename-package-to-kypo-topology-graph' into 'master'
|\  
| * f22bbe7 -- Resolve "Rename package to @kypo/topology-graph"
|/  
*   973f029 -- Merge branch '38-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * fa1b651 -- Remove personal info from README
|/  
*   aae734f -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 41bbf78 -- Resolve "Use Cypress image in CI"
|/  
* ae4f343 -- Update project package.json version based on GitLab tag. Done by CI
*   19a56f6 -- Merge branch '36-update-to-angualar-10' into 'master'
|\  
| * 4d6a53f -- Resolve "Update to Angular 10"
|/  
* bb653af -- Update project package.json version based on GitLab tag. Done by CI
### 14.3.3 Fix docker display rule, enhance the legend display
* 4f021bd -- [CI/CD] Update packages.json version based on GitLab tag.
*   1a05ae1 -- Merge branch '71-fix-docker-icon-and-legend' into 'master'
|\  
| * be8b0d0 -- Fix docker icon and legend
|/  
* 5539f53 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4b78fd9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ff9fc40 -- Merge branch '70-add-support-for-docker-containers-in-topology' into 'master'
|\  
| * 19559f5 -- Resolve "Add support for Docker containers in topology"
|/  
* a92f56a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b2049f3 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e20edb7 -- Merge branch '69-fix-the-number-of-hidden-subnodes' into 'master'
|\  
| * 8c3e11e -- Resolve "Fix the number of hidden subnodes"
|/  
* 548a15c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cd2bd81 -- [CI/CD] Update packages.json version based on GitLab tag.
*   cd8710f -- Merge branch 'fix-guacamole' into 'master'
|\  
| * 352bfa7 -- Fix guacamole uuid
|/  
* f7f78b6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cd7a984 -- [CI/CD] Update packages.json version based on GitLab tag.
*   155455d -- Merge branch '68-change-sandbox-service-endpoints-that-use-number-data-type-to-string-for-sandbox-uuids' into 'master'
|\  
| * a737470 -- Resolve "Change sandbox service endpoints that use number data type to string for sandbox uuids"
|/  
* 762bc2a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cd52622 -- [CI/CD] Update packages.json version based on GitLab tag.
*   5a03e5b -- Merge branch '67-disable-spice-console-when-it-is-not-ready' into 'master'
|\  
| * bce7dc0 -- Resolve "Disable spice console when it is not ready"
|/  
*   2ac86af -- Merge branch '66-rework-drop-down-menu-in-topology' into 'master'
|\  
| * ce949cb -- Resolve "Rework drop down menu in topology"
|/  
* f10bc7b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 45780fd -- [CI/CD] Update packages.json version based on GitLab tag.
*   0d70187 -- Merge branch '65-rename-from-kypo2-to-kypo' into 'master'
|\  
| * 8e8d954 -- Resolve "Rename from kypo2 to kypo"
|/  
* d4b6392 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2276695 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ba17f3c -- Merge branch '64-fix-imports' into 'master'
|\  
| * 6d25f0e -- Resolve "Fix imports"
|/  
* 347f05e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 130df10 -- [CI/CD] Update packages.json version based on GitLab tag.
*   89e4c19 -- Merge branch '63-update-to-angular-14' into 'master'
|\  
| * 51310b6 -- Resolve "Update to Angular 14"
|/  
*   ac8272f -- Merge branch '62-fix-context-menu-clipping' into 'master'
|\  
| * c5ae6b4 -- Resolve "Fix context menu clipping"
|/  
* aedd30f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* ae7bb99 -- [CI/CD] Update packages.json version based on GitLab tag.
*   452a279 -- Merge branch 'fix-context-menu' into 'master'
|\  
| * 665d6ac -- Fix business logic of context menu
|/  
* 3cc2eec -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 16ecabb -- [CI/CD] Update packages.json version based on GitLab tag.
*   723cfad -- Merge branch '61-review-open-console-options-and-remove-resume-option' into 'master'
|\  
| * eec6e04 -- Removed Resume option from node menu. Renamed open console options.
|/  
* 16aa303 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f6e87de -- [CI/CD] Update packages.json version based on GitLab tag.
*   3e0770d -- Merge branch '60-update-to-angular-13' into 'master'
|\  
| * 08e1839 -- Resolve "Update to Angular 13"
|/  
*   9a43908 -- Merge branch '59-display-the-topology-based-on-the-sandbox-definition-id' into 'master'
|\  
| * 391a95f -- Resolve "Display the topology based on the sandbox definition ID"
|/  
* 4d6b41a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fe31cfd -- [CI/CD] Update packages.json version based on GitLab tag.
*   3faed73 -- Merge branch '58-bump-version' into 'master'
|\  
| * 292abe6 -- Bump version
|/  
*   8410e87 -- Merge branch '57-fix-auto-topology-expansion-with-small-number-of-nodes' into 'master'
|\  
| * 59e0050 -- Move topology expansion decision to visuals
|/  
* b1c971c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* edf8c00 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7773b1c -- Merge branch '56-bump-version-of-d3' into 'master'
|\  
| * e5e7fb2 -- Resolve "Bump version of d3"
|/  
* ba84d32 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 52c67ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   e3a5ddf -- Merge branch '55-add-attributes-gui_access-and-os_type-to-routerdto' into 'master'
|\  
| * 57823ce -- Add missing attributes. Modified mapper accordingly.
|/  
* 37d0f58 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b31153f -- [CI/CD] Update packages.json version based on GitLab tag.
*   ecbd2f5 -- Merge branch '53-dispaly-open-cli-and-open-gui-option-based-on-the-os-type-and-guiaccess-attribute-of-the-host' into 'master'
|\  
| * bd8032c -- Resolve "Dispaly Open CLI and Open GUI option based on the OS type and guiAccess attribute of the host"
|/  
*   d683ace -- Merge branch '54-add-console-preload' into 'master'
|\  
| * 69439f1 -- Resolve "Add console preload"
|/  
*   b740a39 -- Merge branch '51-switching-the-host-console-access-to-a-single-phase' into 'master'
|\  
| * 3d4a8f1 -- Remove loading of spice console
|/  
* 9b52172 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 340fa12 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e42f178 -- Merge branch '50-add-simple-legend-for-topology-nodes' into 'master'
|\  
| * 344b252 -- Resolve "Add simple legend for topology nodes"
|/  
*   cf7e3b1 -- Merge branch '52-add-license-file' into 'master'
|\  
| * 2fd2ad6 -- Add license file
|/  
*   1e4a62f -- Merge branch '49-update-the-look-of-topology-nodes' into 'master'
|\  
| * 00fad21 -- Update nodes visibility and icons
|/  
* a9aa75b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 73354b2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   34da99c -- Merge branch '48-add-option-to-open-console-using-apache-guacamole' into 'master'
|\  
| * 916ef3a -- Resolve "Add option to open console using apache guacamole"
|/  
* b8b6b33 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 70fd3a9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8a59576 -- Merge branch '47-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * b6a4002 -- Update gitlab CI
|/  
* a291334 -- Update project package.json version based on GitLab tag. Done by CI
*   65ea149 -- Merge branch '46-update-to-angular-12' into 'master'
|\  
| * cbe2590 -- Resolve "Update to Angular 12"
|/  
* 9e6fcf5 -- Update project package.json version based on GitLab tag. Done by CI
*   4654845 -- Merge branch '45-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0aeb4a5 -- Update peerDependencies and dependencies
|/  
*   ab1fdb6 -- Merge branch '44-update-to-angular-11' into 'master'
|\  
| * 90fe8a8 -- Resolve "Update to Angular 11"
|/  
*   2f94029 -- Merge branch '43-recreate-package-lock-for-new-package-registry-muni-kypo-crp-frontend-angular-models-kypo' into 'master'
|\  
| * b8c9c07 -- reacreate package lock
|/  
*   6db319a -- Merge branch '42-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 7fe3c8b -- Resolve "Migrate from tslint to eslint"
|/  
* af94615 -- Update project package.json version based on GitLab tag. Done by CI
*   105c759 -- Merge branch '41-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * eebd0de -- rename package scope and update dependencies
|/  
* 5f4642b -- Update project package.json version based on GitLab tag. Done by CI
*   6d4d5ea -- Merge branch '40-update-dependencies-to-new-format' into 'master'
|\  
| * a97e66c -- Resolve "Update dependencies to new format"
|/  
* 3fe51b8 -- Update project package.json version based on GitLab tag. Done by CI
*   670bd06 -- Merge branch '39-rename-package-to-kypo-topology-graph' into 'master'
|\  
| * f22bbe7 -- Resolve "Rename package to @kypo/topology-graph"
|/  
*   973f029 -- Merge branch '38-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * fa1b651 -- Remove personal info from README
|/  
*   aae734f -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 41bbf78 -- Resolve "Use Cypress image in CI"
|/  
* ae4f343 -- Update project package.json version based on GitLab tag. Done by CI
*   19a56f6 -- Merge branch '36-update-to-angualar-10' into 'master'
|\  
| * 4d6a53f -- Resolve "Update to Angular 10"
|/  
* bb653af -- Update project package.json version based on GitLab tag. Done by CI
### 14.3.2 Update topology appearance with new docker container nodes, add legend
* 4b78fd9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ff9fc40 -- Merge branch '70-add-support-for-docker-containers-in-topology' into 'master'
|\  
| * 19559f5 -- Resolve "Add support for Docker containers in topology"
|/  
* a92f56a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b2049f3 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e20edb7 -- Merge branch '69-fix-the-number-of-hidden-subnodes' into 'master'
|\  
| * 8c3e11e -- Resolve "Fix the number of hidden subnodes"
|/  
* 548a15c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cd2bd81 -- [CI/CD] Update packages.json version based on GitLab tag.
*   cd8710f -- Merge branch 'fix-guacamole' into 'master'
|\  
| * 352bfa7 -- Fix guacamole uuid
|/  
* f7f78b6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cd7a984 -- [CI/CD] Update packages.json version based on GitLab tag.
*   155455d -- Merge branch '68-change-sandbox-service-endpoints-that-use-number-data-type-to-string-for-sandbox-uuids' into 'master'
|\  
| * a737470 -- Resolve "Change sandbox service endpoints that use number data type to string for sandbox uuids"
|/  
* 762bc2a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cd52622 -- [CI/CD] Update packages.json version based on GitLab tag.
*   5a03e5b -- Merge branch '67-disable-spice-console-when-it-is-not-ready' into 'master'
|\  
| * bce7dc0 -- Resolve "Disable spice console when it is not ready"
|/  
*   2ac86af -- Merge branch '66-rework-drop-down-menu-in-topology' into 'master'
|\  
| * ce949cb -- Resolve "Rework drop down menu in topology"
|/  
* f10bc7b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 45780fd -- [CI/CD] Update packages.json version based on GitLab tag.
*   0d70187 -- Merge branch '65-rename-from-kypo2-to-kypo' into 'master'
|\  
| * 8e8d954 -- Resolve "Rename from kypo2 to kypo"
|/  
* d4b6392 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2276695 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ba17f3c -- Merge branch '64-fix-imports' into 'master'
|\  
| * 6d25f0e -- Resolve "Fix imports"
|/  
* 347f05e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 130df10 -- [CI/CD] Update packages.json version based on GitLab tag.
*   89e4c19 -- Merge branch '63-update-to-angular-14' into 'master'
|\  
| * 51310b6 -- Resolve "Update to Angular 14"
|/  
*   ac8272f -- Merge branch '62-fix-context-menu-clipping' into 'master'
|\  
| * c5ae6b4 -- Resolve "Fix context menu clipping"
|/  
* aedd30f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* ae7bb99 -- [CI/CD] Update packages.json version based on GitLab tag.
*   452a279 -- Merge branch 'fix-context-menu' into 'master'
|\  
| * 665d6ac -- Fix business logic of context menu
|/  
* 3cc2eec -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 16ecabb -- [CI/CD] Update packages.json version based on GitLab tag.
*   723cfad -- Merge branch '61-review-open-console-options-and-remove-resume-option' into 'master'
|\  
| * eec6e04 -- Removed Resume option from node menu. Renamed open console options.
|/  
* 16aa303 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f6e87de -- [CI/CD] Update packages.json version based on GitLab tag.
*   3e0770d -- Merge branch '60-update-to-angular-13' into 'master'
|\  
| * 08e1839 -- Resolve "Update to Angular 13"
|/  
*   9a43908 -- Merge branch '59-display-the-topology-based-on-the-sandbox-definition-id' into 'master'
|\  
| * 391a95f -- Resolve "Display the topology based on the sandbox definition ID"
|/  
* 4d6b41a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fe31cfd -- [CI/CD] Update packages.json version based on GitLab tag.
*   3faed73 -- Merge branch '58-bump-version' into 'master'
|\  
| * 292abe6 -- Bump version
|/  
*   8410e87 -- Merge branch '57-fix-auto-topology-expansion-with-small-number-of-nodes' into 'master'
|\  
| * 59e0050 -- Move topology expansion decision to visuals
|/  
* b1c971c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* edf8c00 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7773b1c -- Merge branch '56-bump-version-of-d3' into 'master'
|\  
| * e5e7fb2 -- Resolve "Bump version of d3"
|/  
* ba84d32 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 52c67ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   e3a5ddf -- Merge branch '55-add-attributes-gui_access-and-os_type-to-routerdto' into 'master'
|\  
| * 57823ce -- Add missing attributes. Modified mapper accordingly.
|/  
* 37d0f58 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b31153f -- [CI/CD] Update packages.json version based on GitLab tag.
*   ecbd2f5 -- Merge branch '53-dispaly-open-cli-and-open-gui-option-based-on-the-os-type-and-guiaccess-attribute-of-the-host' into 'master'
|\  
| * bd8032c -- Resolve "Dispaly Open CLI and Open GUI option based on the OS type and guiAccess attribute of the host"
|/  
*   d683ace -- Merge branch '54-add-console-preload' into 'master'
|\  
| * 69439f1 -- Resolve "Add console preload"
|/  
*   b740a39 -- Merge branch '51-switching-the-host-console-access-to-a-single-phase' into 'master'
|\  
| * 3d4a8f1 -- Remove loading of spice console
|/  
* 9b52172 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 340fa12 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e42f178 -- Merge branch '50-add-simple-legend-for-topology-nodes' into 'master'
|\  
| * 344b252 -- Resolve "Add simple legend for topology nodes"
|/  
*   cf7e3b1 -- Merge branch '52-add-license-file' into 'master'
|\  
| * 2fd2ad6 -- Add license file
|/  
*   1e4a62f -- Merge branch '49-update-the-look-of-topology-nodes' into 'master'
|\  
| * 00fad21 -- Update nodes visibility and icons
|/  
* a9aa75b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 73354b2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   34da99c -- Merge branch '48-add-option-to-open-console-using-apache-guacamole' into 'master'
|\  
| * 916ef3a -- Resolve "Add option to open console using apache guacamole"
|/  
* b8b6b33 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 70fd3a9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8a59576 -- Merge branch '47-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * b6a4002 -- Update gitlab CI
|/  
* a291334 -- Update project package.json version based on GitLab tag. Done by CI
*   65ea149 -- Merge branch '46-update-to-angular-12' into 'master'
|\  
| * cbe2590 -- Resolve "Update to Angular 12"
|/  
* 9e6fcf5 -- Update project package.json version based on GitLab tag. Done by CI
*   4654845 -- Merge branch '45-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0aeb4a5 -- Update peerDependencies and dependencies
|/  
*   ab1fdb6 -- Merge branch '44-update-to-angular-11' into 'master'
|\  
| * 90fe8a8 -- Resolve "Update to Angular 11"
|/  
*   2f94029 -- Merge branch '43-recreate-package-lock-for-new-package-registry-muni-kypo-crp-frontend-angular-models-kypo' into 'master'
|\  
| * b8c9c07 -- reacreate package lock
|/  
*   6db319a -- Merge branch '42-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 7fe3c8b -- Resolve "Migrate from tslint to eslint"
|/  
* af94615 -- Update project package.json version based on GitLab tag. Done by CI
*   105c759 -- Merge branch '41-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * eebd0de -- rename package scope and update dependencies
|/  
* 5f4642b -- Update project package.json version based on GitLab tag. Done by CI
*   6d4d5ea -- Merge branch '40-update-dependencies-to-new-format' into 'master'
|\  
| * a97e66c -- Resolve "Update dependencies to new format"
|/  
* 3fe51b8 -- Update project package.json version based on GitLab tag. Done by CI
*   670bd06 -- Merge branch '39-rename-package-to-kypo-topology-graph' into 'master'
|\  
| * f22bbe7 -- Resolve "Rename package to @kypo/topology-graph"
|/  
*   973f029 -- Merge branch '38-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * fa1b651 -- Remove personal info from README
|/  
*   aae734f -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 41bbf78 -- Resolve "Use Cypress image in CI"
|/  
* ae4f343 -- Update project package.json version based on GitLab tag. Done by CI
*   19a56f6 -- Merge branch '36-update-to-angualar-10' into 'master'
|\  
| * 4d6a53f -- Resolve "Update to Angular 10"
|/  
* bb653af -- Update project package.json version based on GitLab tag. Done by CI
### 14.3.1 Fix collapsed subnet size number.
* b2049f3 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e20edb7 -- Merge branch '69-fix-the-number-of-hidden-subnodes' into 'master'
|\  
| * 8c3e11e -- Resolve "Fix the number of hidden subnodes"
|/  
* 548a15c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cd2bd81 -- [CI/CD] Update packages.json version based on GitLab tag.
*   cd8710f -- Merge branch 'fix-guacamole' into 'master'
|\  
| * 352bfa7 -- Fix guacamole uuid
|/  
* f7f78b6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cd7a984 -- [CI/CD] Update packages.json version based on GitLab tag.
*   155455d -- Merge branch '68-change-sandbox-service-endpoints-that-use-number-data-type-to-string-for-sandbox-uuids' into 'master'
|\  
| * a737470 -- Resolve "Change sandbox service endpoints that use number data type to string for sandbox uuids"
|/  
* 762bc2a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cd52622 -- [CI/CD] Update packages.json version based on GitLab tag.
*   5a03e5b -- Merge branch '67-disable-spice-console-when-it-is-not-ready' into 'master'
|\  
| * bce7dc0 -- Resolve "Disable spice console when it is not ready"
|/  
*   2ac86af -- Merge branch '66-rework-drop-down-menu-in-topology' into 'master'
|\  
| * ce949cb -- Resolve "Rework drop down menu in topology"
|/  
* f10bc7b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 45780fd -- [CI/CD] Update packages.json version based on GitLab tag.
*   0d70187 -- Merge branch '65-rename-from-kypo2-to-kypo' into 'master'
|\  
| * 8e8d954 -- Resolve "Rename from kypo2 to kypo"
|/  
* d4b6392 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2276695 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ba17f3c -- Merge branch '64-fix-imports' into 'master'
|\  
| * 6d25f0e -- Resolve "Fix imports"
|/  
* 347f05e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 130df10 -- [CI/CD] Update packages.json version based on GitLab tag.
*   89e4c19 -- Merge branch '63-update-to-angular-14' into 'master'
|\  
| * 51310b6 -- Resolve "Update to Angular 14"
|/  
*   ac8272f -- Merge branch '62-fix-context-menu-clipping' into 'master'
|\  
| * c5ae6b4 -- Resolve "Fix context menu clipping"
|/  
* aedd30f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* ae7bb99 -- [CI/CD] Update packages.json version based on GitLab tag.
*   452a279 -- Merge branch 'fix-context-menu' into 'master'
|\  
| * 665d6ac -- Fix business logic of context menu
|/  
* 3cc2eec -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 16ecabb -- [CI/CD] Update packages.json version based on GitLab tag.
*   723cfad -- Merge branch '61-review-open-console-options-and-remove-resume-option' into 'master'
|\  
| * eec6e04 -- Removed Resume option from node menu. Renamed open console options.
|/  
* 16aa303 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f6e87de -- [CI/CD] Update packages.json version based on GitLab tag.
*   3e0770d -- Merge branch '60-update-to-angular-13' into 'master'
|\  
| * 08e1839 -- Resolve "Update to Angular 13"
|/  
*   9a43908 -- Merge branch '59-display-the-topology-based-on-the-sandbox-definition-id' into 'master'
|\  
| * 391a95f -- Resolve "Display the topology based on the sandbox definition ID"
|/  
* 4d6b41a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fe31cfd -- [CI/CD] Update packages.json version based on GitLab tag.
*   3faed73 -- Merge branch '58-bump-version' into 'master'
|\  
| * 292abe6 -- Bump version
|/  
*   8410e87 -- Merge branch '57-fix-auto-topology-expansion-with-small-number-of-nodes' into 'master'
|\  
| * 59e0050 -- Move topology expansion decision to visuals
|/  
* b1c971c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* edf8c00 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7773b1c -- Merge branch '56-bump-version-of-d3' into 'master'
|\  
| * e5e7fb2 -- Resolve "Bump version of d3"
|/  
* ba84d32 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 52c67ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   e3a5ddf -- Merge branch '55-add-attributes-gui_access-and-os_type-to-routerdto' into 'master'
|\  
| * 57823ce -- Add missing attributes. Modified mapper accordingly.
|/  
* 37d0f58 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b31153f -- [CI/CD] Update packages.json version based on GitLab tag.
*   ecbd2f5 -- Merge branch '53-dispaly-open-cli-and-open-gui-option-based-on-the-os-type-and-guiaccess-attribute-of-the-host' into 'master'
|\  
| * bd8032c -- Resolve "Dispaly Open CLI and Open GUI option based on the OS type and guiAccess attribute of the host"
|/  
*   d683ace -- Merge branch '54-add-console-preload' into 'master'
|\  
| * 69439f1 -- Resolve "Add console preload"
|/  
*   b740a39 -- Merge branch '51-switching-the-host-console-access-to-a-single-phase' into 'master'
|\  
| * 3d4a8f1 -- Remove loading of spice console
|/  
* 9b52172 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 340fa12 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e42f178 -- Merge branch '50-add-simple-legend-for-topology-nodes' into 'master'
|\  
| * 344b252 -- Resolve "Add simple legend for topology nodes"
|/  
*   cf7e3b1 -- Merge branch '52-add-license-file' into 'master'
|\  
| * 2fd2ad6 -- Add license file
|/  
*   1e4a62f -- Merge branch '49-update-the-look-of-topology-nodes' into 'master'
|\  
| * 00fad21 -- Update nodes visibility and icons
|/  
* a9aa75b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 73354b2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   34da99c -- Merge branch '48-add-option-to-open-console-using-apache-guacamole' into 'master'
|\  
| * 916ef3a -- Resolve "Add option to open console using apache guacamole"
|/  
* b8b6b33 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 70fd3a9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8a59576 -- Merge branch '47-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * b6a4002 -- Update gitlab CI
|/  
* a291334 -- Update project package.json version based on GitLab tag. Done by CI
*   65ea149 -- Merge branch '46-update-to-angular-12' into 'master'
|\  
| * cbe2590 -- Resolve "Update to Angular 12"
|/  
* 9e6fcf5 -- Update project package.json version based on GitLab tag. Done by CI
*   4654845 -- Merge branch '45-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0aeb4a5 -- Update peerDependencies and dependencies
|/  
*   ab1fdb6 -- Merge branch '44-update-to-angular-11' into 'master'
|\  
| * 90fe8a8 -- Resolve "Update to Angular 11"
|/  
*   2f94029 -- Merge branch '43-recreate-package-lock-for-new-package-registry-muni-kypo-crp-frontend-angular-models-kypo' into 'master'
|\  
| * b8c9c07 -- reacreate package lock
|/  
*   6db319a -- Merge branch '42-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 7fe3c8b -- Resolve "Migrate from tslint to eslint"
|/  
* af94615 -- Update project package.json version based on GitLab tag. Done by CI
*   105c759 -- Merge branch '41-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * eebd0de -- rename package scope and update dependencies
|/  
* 5f4642b -- Update project package.json version based on GitLab tag. Done by CI
*   6d4d5ea -- Merge branch '40-update-dependencies-to-new-format' into 'master'
|\  
| * a97e66c -- Resolve "Update dependencies to new format"
|/  
* 3fe51b8 -- Update project package.json version based on GitLab tag. Done by CI
*   670bd06 -- Merge branch '39-rename-package-to-kypo-topology-graph' into 'master'
|\  
| * f22bbe7 -- Resolve "Rename package to @kypo/topology-graph"
|/  
*   973f029 -- Merge branch '38-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * fa1b651 -- Remove personal info from README
|/  
*   aae734f -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 41bbf78 -- Resolve "Use Cypress image in CI"
|/  
* ae4f343 -- Update project package.json version based on GitLab tag. Done by CI
*   19a56f6 -- Merge branch '36-update-to-angualar-10' into 'master'
|\  
| * 4d6a53f -- Resolve "Update to Angular 10"
|/  
* bb653af -- Update project package.json version based on GitLab tag. Done by CI
### 14.3.0 Fix uuid for guacamole access.
* cd2bd81 -- [CI/CD] Update packages.json version based on GitLab tag.
*   cd8710f -- Merge branch 'fix-guacamole' into 'master'
|\  
| * 352bfa7 -- Fix guacamole uuid
|/  
* f7f78b6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cd7a984 -- [CI/CD] Update packages.json version based on GitLab tag.
*   155455d -- Merge branch '68-change-sandbox-service-endpoints-that-use-number-data-type-to-string-for-sandbox-uuids' into 'master'
|\  
| * a737470 -- Resolve "Change sandbox service endpoints that use number data type to string for sandbox uuids"
|/  
* 762bc2a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cd52622 -- [CI/CD] Update packages.json version based on GitLab tag.
*   5a03e5b -- Merge branch '67-disable-spice-console-when-it-is-not-ready' into 'master'
|\  
| * bce7dc0 -- Resolve "Disable spice console when it is not ready"
|/  
*   2ac86af -- Merge branch '66-rework-drop-down-menu-in-topology' into 'master'
|\  
| * ce949cb -- Resolve "Rework drop down menu in topology"
|/  
* f10bc7b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 45780fd -- [CI/CD] Update packages.json version based on GitLab tag.
*   0d70187 -- Merge branch '65-rename-from-kypo2-to-kypo' into 'master'
|\  
| * 8e8d954 -- Resolve "Rename from kypo2 to kypo"
|/  
* d4b6392 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2276695 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ba17f3c -- Merge branch '64-fix-imports' into 'master'
|\  
| * 6d25f0e -- Resolve "Fix imports"
|/  
* 347f05e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 130df10 -- [CI/CD] Update packages.json version based on GitLab tag.
*   89e4c19 -- Merge branch '63-update-to-angular-14' into 'master'
|\  
| * 51310b6 -- Resolve "Update to Angular 14"
|/  
*   ac8272f -- Merge branch '62-fix-context-menu-clipping' into 'master'
|\  
| * c5ae6b4 -- Resolve "Fix context menu clipping"
|/  
* aedd30f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* ae7bb99 -- [CI/CD] Update packages.json version based on GitLab tag.
*   452a279 -- Merge branch 'fix-context-menu' into 'master'
|\  
| * 665d6ac -- Fix business logic of context menu
|/  
* 3cc2eec -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 16ecabb -- [CI/CD] Update packages.json version based on GitLab tag.
*   723cfad -- Merge branch '61-review-open-console-options-and-remove-resume-option' into 'master'
|\  
| * eec6e04 -- Removed Resume option from node menu. Renamed open console options.
|/  
* 16aa303 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f6e87de -- [CI/CD] Update packages.json version based on GitLab tag.
*   3e0770d -- Merge branch '60-update-to-angular-13' into 'master'
|\  
| * 08e1839 -- Resolve "Update to Angular 13"
|/  
*   9a43908 -- Merge branch '59-display-the-topology-based-on-the-sandbox-definition-id' into 'master'
|\  
| * 391a95f -- Resolve "Display the topology based on the sandbox definition ID"
|/  
* 4d6b41a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fe31cfd -- [CI/CD] Update packages.json version based on GitLab tag.
*   3faed73 -- Merge branch '58-bump-version' into 'master'
|\  
| * 292abe6 -- Bump version
|/  
*   8410e87 -- Merge branch '57-fix-auto-topology-expansion-with-small-number-of-nodes' into 'master'
|\  
| * 59e0050 -- Move topology expansion decision to visuals
|/  
* b1c971c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* edf8c00 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7773b1c -- Merge branch '56-bump-version-of-d3' into 'master'
|\  
| * e5e7fb2 -- Resolve "Bump version of d3"
|/  
* ba84d32 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 52c67ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   e3a5ddf -- Merge branch '55-add-attributes-gui_access-and-os_type-to-routerdto' into 'master'
|\  
| * 57823ce -- Add missing attributes. Modified mapper accordingly.
|/  
* 37d0f58 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b31153f -- [CI/CD] Update packages.json version based on GitLab tag.
*   ecbd2f5 -- Merge branch '53-dispaly-open-cli-and-open-gui-option-based-on-the-os-type-and-guiaccess-attribute-of-the-host' into 'master'
|\  
| * bd8032c -- Resolve "Dispaly Open CLI and Open GUI option based on the OS type and guiAccess attribute of the host"
|/  
*   d683ace -- Merge branch '54-add-console-preload' into 'master'
|\  
| * 69439f1 -- Resolve "Add console preload"
|/  
*   b740a39 -- Merge branch '51-switching-the-host-console-access-to-a-single-phase' into 'master'
|\  
| * 3d4a8f1 -- Remove loading of spice console
|/  
* 9b52172 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 340fa12 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e42f178 -- Merge branch '50-add-simple-legend-for-topology-nodes' into 'master'
|\  
| * 344b252 -- Resolve "Add simple legend for topology nodes"
|/  
*   cf7e3b1 -- Merge branch '52-add-license-file' into 'master'
|\  
| * 2fd2ad6 -- Add license file
|/  
*   1e4a62f -- Merge branch '49-update-the-look-of-topology-nodes' into 'master'
|\  
| * 00fad21 -- Update nodes visibility and icons
|/  
* a9aa75b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 73354b2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   34da99c -- Merge branch '48-add-option-to-open-console-using-apache-guacamole' into 'master'
|\  
| * 916ef3a -- Resolve "Add option to open console using apache guacamole"
|/  
* b8b6b33 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 70fd3a9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8a59576 -- Merge branch '47-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * b6a4002 -- Update gitlab CI
|/  
* a291334 -- Update project package.json version based on GitLab tag. Done by CI
*   65ea149 -- Merge branch '46-update-to-angular-12' into 'master'
|\  
| * cbe2590 -- Resolve "Update to Angular 12"
|/  
* 9e6fcf5 -- Update project package.json version based on GitLab tag. Done by CI
*   4654845 -- Merge branch '45-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0aeb4a5 -- Update peerDependencies and dependencies
|/  
*   ab1fdb6 -- Merge branch '44-update-to-angular-11' into 'master'
|\  
| * 90fe8a8 -- Resolve "Update to Angular 11"
|/  
*   2f94029 -- Merge branch '43-recreate-package-lock-for-new-package-registry-muni-kypo-crp-frontend-angular-models-kypo' into 'master'
|\  
| * b8c9c07 -- reacreate package lock
|/  
*   6db319a -- Merge branch '42-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 7fe3c8b -- Resolve "Migrate from tslint to eslint"
|/  
* af94615 -- Update project package.json version based on GitLab tag. Done by CI
*   105c759 -- Merge branch '41-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * eebd0de -- rename package scope and update dependencies
|/  
* 5f4642b -- Update project package.json version based on GitLab tag. Done by CI
*   6d4d5ea -- Merge branch '40-update-dependencies-to-new-format' into 'master'
|\  
| * a97e66c -- Resolve "Update dependencies to new format"
|/  
* 3fe51b8 -- Update project package.json version based on GitLab tag. Done by CI
*   670bd06 -- Merge branch '39-rename-package-to-kypo-topology-graph' into 'master'
|\  
| * f22bbe7 -- Resolve "Rename package to @kypo/topology-graph"
|/  
*   973f029 -- Merge branch '38-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * fa1b651 -- Remove personal info from README
|/  
*   aae734f -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 41bbf78 -- Resolve "Use Cypress image in CI"
|/  
* ae4f343 -- Update project package.json version based on GitLab tag. Done by CI
*   19a56f6 -- Merge branch '36-update-to-angualar-10' into 'master'
|\  
| * 4d6a53f -- Resolve "Update to Angular 10"
|/  
* bb653af -- Update project package.json version based on GitLab tag. Done by CI
### 14.2.0 Replace sandbox id with uuid.
* cd7a984 -- [CI/CD] Update packages.json version based on GitLab tag.
*   155455d -- Merge branch '68-change-sandbox-service-endpoints-that-use-number-data-type-to-string-for-sandbox-uuids' into 'master'
|\  
| * a737470 -- Resolve "Change sandbox service endpoints that use number data type to string for sandbox uuids"
|/  
* 762bc2a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cd52622 -- [CI/CD] Update packages.json version based on GitLab tag.
*   5a03e5b -- Merge branch '67-disable-spice-console-when-it-is-not-ready' into 'master'
|\  
| * bce7dc0 -- Resolve "Disable spice console when it is not ready"
|/  
*   2ac86af -- Merge branch '66-rework-drop-down-menu-in-topology' into 'master'
|\  
| * ce949cb -- Resolve "Rework drop down menu in topology"
|/  
* f10bc7b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 45780fd -- [CI/CD] Update packages.json version based on GitLab tag.
*   0d70187 -- Merge branch '65-rename-from-kypo2-to-kypo' into 'master'
|\  
| * 8e8d954 -- Resolve "Rename from kypo2 to kypo"
|/  
* d4b6392 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2276695 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ba17f3c -- Merge branch '64-fix-imports' into 'master'
|\  
| * 6d25f0e -- Resolve "Fix imports"
|/  
* 347f05e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 130df10 -- [CI/CD] Update packages.json version based on GitLab tag.
*   89e4c19 -- Merge branch '63-update-to-angular-14' into 'master'
|\  
| * 51310b6 -- Resolve "Update to Angular 14"
|/  
*   ac8272f -- Merge branch '62-fix-context-menu-clipping' into 'master'
|\  
| * c5ae6b4 -- Resolve "Fix context menu clipping"
|/  
* aedd30f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* ae7bb99 -- [CI/CD] Update packages.json version based on GitLab tag.
*   452a279 -- Merge branch 'fix-context-menu' into 'master'
|\  
| * 665d6ac -- Fix business logic of context menu
|/  
* 3cc2eec -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 16ecabb -- [CI/CD] Update packages.json version based on GitLab tag.
*   723cfad -- Merge branch '61-review-open-console-options-and-remove-resume-option' into 'master'
|\  
| * eec6e04 -- Removed Resume option from node menu. Renamed open console options.
|/  
* 16aa303 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f6e87de -- [CI/CD] Update packages.json version based on GitLab tag.
*   3e0770d -- Merge branch '60-update-to-angular-13' into 'master'
|\  
| * 08e1839 -- Resolve "Update to Angular 13"
|/  
*   9a43908 -- Merge branch '59-display-the-topology-based-on-the-sandbox-definition-id' into 'master'
|\  
| * 391a95f -- Resolve "Display the topology based on the sandbox definition ID"
|/  
* 4d6b41a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fe31cfd -- [CI/CD] Update packages.json version based on GitLab tag.
*   3faed73 -- Merge branch '58-bump-version' into 'master'
|\  
| * 292abe6 -- Bump version
|/  
*   8410e87 -- Merge branch '57-fix-auto-topology-expansion-with-small-number-of-nodes' into 'master'
|\  
| * 59e0050 -- Move topology expansion decision to visuals
|/  
* b1c971c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* edf8c00 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7773b1c -- Merge branch '56-bump-version-of-d3' into 'master'
|\  
| * e5e7fb2 -- Resolve "Bump version of d3"
|/  
* ba84d32 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 52c67ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   e3a5ddf -- Merge branch '55-add-attributes-gui_access-and-os_type-to-routerdto' into 'master'
|\  
| * 57823ce -- Add missing attributes. Modified mapper accordingly.
|/  
* 37d0f58 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b31153f -- [CI/CD] Update packages.json version based on GitLab tag.
*   ecbd2f5 -- Merge branch '53-dispaly-open-cli-and-open-gui-option-based-on-the-os-type-and-guiaccess-attribute-of-the-host' into 'master'
|\  
| * bd8032c -- Resolve "Dispaly Open CLI and Open GUI option based on the OS type and guiAccess attribute of the host"
|/  
*   d683ace -- Merge branch '54-add-console-preload' into 'master'
|\  
| * 69439f1 -- Resolve "Add console preload"
|/  
*   b740a39 -- Merge branch '51-switching-the-host-console-access-to-a-single-phase' into 'master'
|\  
| * 3d4a8f1 -- Remove loading of spice console
|/  
* 9b52172 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 340fa12 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e42f178 -- Merge branch '50-add-simple-legend-for-topology-nodes' into 'master'
|\  
| * 344b252 -- Resolve "Add simple legend for topology nodes"
|/  
*   cf7e3b1 -- Merge branch '52-add-license-file' into 'master'
|\  
| * 2fd2ad6 -- Add license file
|/  
*   1e4a62f -- Merge branch '49-update-the-look-of-topology-nodes' into 'master'
|\  
| * 00fad21 -- Update nodes visibility and icons
|/  
* a9aa75b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 73354b2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   34da99c -- Merge branch '48-add-option-to-open-console-using-apache-guacamole' into 'master'
|\  
| * 916ef3a -- Resolve "Add option to open console using apache guacamole"
|/  
* b8b6b33 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 70fd3a9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8a59576 -- Merge branch '47-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * b6a4002 -- Update gitlab CI
|/  
* a291334 -- Update project package.json version based on GitLab tag. Done by CI
*   65ea149 -- Merge branch '46-update-to-angular-12' into 'master'
|\  
| * cbe2590 -- Resolve "Update to Angular 12"
|/  
* 9e6fcf5 -- Update project package.json version based on GitLab tag. Done by CI
*   4654845 -- Merge branch '45-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0aeb4a5 -- Update peerDependencies and dependencies
|/  
*   ab1fdb6 -- Merge branch '44-update-to-angular-11' into 'master'
|\  
| * 90fe8a8 -- Resolve "Update to Angular 11"
|/  
*   2f94029 -- Merge branch '43-recreate-package-lock-for-new-package-registry-muni-kypo-crp-frontend-angular-models-kypo' into 'master'
|\  
| * b8c9c07 -- reacreate package lock
|/  
*   6db319a -- Merge branch '42-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 7fe3c8b -- Resolve "Migrate from tslint to eslint"
|/  
* af94615 -- Update project package.json version based on GitLab tag. Done by CI
*   105c759 -- Merge branch '41-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * eebd0de -- rename package scope and update dependencies
|/  
* 5f4642b -- Update project package.json version based on GitLab tag. Done by CI
*   6d4d5ea -- Merge branch '40-update-dependencies-to-new-format' into 'master'
|\  
| * a97e66c -- Resolve "Update dependencies to new format"
|/  
* 3fe51b8 -- Update project package.json version based on GitLab tag. Done by CI
*   670bd06 -- Merge branch '39-rename-package-to-kypo-topology-graph' into 'master'
|\  
| * f22bbe7 -- Resolve "Rename package to @kypo/topology-graph"
|/  
*   973f029 -- Merge branch '38-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * fa1b651 -- Remove personal info from README
|/  
*   aae734f -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 41bbf78 -- Resolve "Use Cypress image in CI"
|/  
* ae4f343 -- Update project package.json version based on GitLab tag. Done by CI
*   19a56f6 -- Merge branch '36-update-to-angualar-10' into 'master'
|\  
| * 4d6a53f -- Resolve "Update to Angular 10"
|/  
* bb653af -- Update project package.json version based on GitLab tag. Done by CI
### 14.1.0 Disabled spice consoles option when it is not ready. Changed strategy for obtaining spice consoles.
* cd52622 -- [CI/CD] Update packages.json version based on GitLab tag.
*   5a03e5b -- Merge branch '67-disable-spice-console-when-it-is-not-ready' into 'master'
|\  
| * bce7dc0 -- Resolve "Disable spice console when it is not ready"
|/  
*   2ac86af -- Merge branch '66-rework-drop-down-menu-in-topology' into 'master'
|\  
| * ce949cb -- Resolve "Rework drop down menu in topology"
|/  
* f10bc7b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 45780fd -- [CI/CD] Update packages.json version based on GitLab tag.
*   0d70187 -- Merge branch '65-rename-from-kypo2-to-kypo' into 'master'
|\  
| * 8e8d954 -- Resolve "Rename from kypo2 to kypo"
|/  
* d4b6392 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2276695 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ba17f3c -- Merge branch '64-fix-imports' into 'master'
|\  
| * 6d25f0e -- Resolve "Fix imports"
|/  
* 347f05e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 130df10 -- [CI/CD] Update packages.json version based on GitLab tag.
*   89e4c19 -- Merge branch '63-update-to-angular-14' into 'master'
|\  
| * 51310b6 -- Resolve "Update to Angular 14"
|/  
*   ac8272f -- Merge branch '62-fix-context-menu-clipping' into 'master'
|\  
| * c5ae6b4 -- Resolve "Fix context menu clipping"
|/  
* aedd30f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* ae7bb99 -- [CI/CD] Update packages.json version based on GitLab tag.
*   452a279 -- Merge branch 'fix-context-menu' into 'master'
|\  
| * 665d6ac -- Fix business logic of context menu
|/  
* 3cc2eec -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 16ecabb -- [CI/CD] Update packages.json version based on GitLab tag.
*   723cfad -- Merge branch '61-review-open-console-options-and-remove-resume-option' into 'master'
|\  
| * eec6e04 -- Removed Resume option from node menu. Renamed open console options.
|/  
* 16aa303 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f6e87de -- [CI/CD] Update packages.json version based on GitLab tag.
*   3e0770d -- Merge branch '60-update-to-angular-13' into 'master'
|\  
| * 08e1839 -- Resolve "Update to Angular 13"
|/  
*   9a43908 -- Merge branch '59-display-the-topology-based-on-the-sandbox-definition-id' into 'master'
|\  
| * 391a95f -- Resolve "Display the topology based on the sandbox definition ID"
|/  
* 4d6b41a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fe31cfd -- [CI/CD] Update packages.json version based on GitLab tag.
*   3faed73 -- Merge branch '58-bump-version' into 'master'
|\  
| * 292abe6 -- Bump version
|/  
*   8410e87 -- Merge branch '57-fix-auto-topology-expansion-with-small-number-of-nodes' into 'master'
|\  
| * 59e0050 -- Move topology expansion decision to visuals
|/  
* b1c971c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* edf8c00 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7773b1c -- Merge branch '56-bump-version-of-d3' into 'master'
|\  
| * e5e7fb2 -- Resolve "Bump version of d3"
|/  
* ba84d32 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 52c67ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   e3a5ddf -- Merge branch '55-add-attributes-gui_access-and-os_type-to-routerdto' into 'master'
|\  
| * 57823ce -- Add missing attributes. Modified mapper accordingly.
|/  
* 37d0f58 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b31153f -- [CI/CD] Update packages.json version based on GitLab tag.
*   ecbd2f5 -- Merge branch '53-dispaly-open-cli-and-open-gui-option-based-on-the-os-type-and-guiaccess-attribute-of-the-host' into 'master'
|\  
| * bd8032c -- Resolve "Dispaly Open CLI and Open GUI option based on the OS type and guiAccess attribute of the host"
|/  
*   d683ace -- Merge branch '54-add-console-preload' into 'master'
|\  
| * 69439f1 -- Resolve "Add console preload"
|/  
*   b740a39 -- Merge branch '51-switching-the-host-console-access-to-a-single-phase' into 'master'
|\  
| * 3d4a8f1 -- Remove loading of spice console
|/  
* 9b52172 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 340fa12 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e42f178 -- Merge branch '50-add-simple-legend-for-topology-nodes' into 'master'
|\  
| * 344b252 -- Resolve "Add simple legend for topology nodes"
|/  
*   cf7e3b1 -- Merge branch '52-add-license-file' into 'master'
|\  
| * 2fd2ad6 -- Add license file
|/  
*   1e4a62f -- Merge branch '49-update-the-look-of-topology-nodes' into 'master'
|\  
| * 00fad21 -- Update nodes visibility and icons
|/  
* a9aa75b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 73354b2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   34da99c -- Merge branch '48-add-option-to-open-console-using-apache-guacamole' into 'master'
|\  
| * 916ef3a -- Resolve "Add option to open console using apache guacamole"
|/  
* b8b6b33 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 70fd3a9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8a59576 -- Merge branch '47-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * b6a4002 -- Update gitlab CI
|/  
* a291334 -- Update project package.json version based on GitLab tag. Done by CI
*   65ea149 -- Merge branch '46-update-to-angular-12' into 'master'
|\  
| * cbe2590 -- Resolve "Update to Angular 12"
|/  
* 9e6fcf5 -- Update project package.json version based on GitLab tag. Done by CI
*   4654845 -- Merge branch '45-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0aeb4a5 -- Update peerDependencies and dependencies
|/  
*   ab1fdb6 -- Merge branch '44-update-to-angular-11' into 'master'
|\  
| * 90fe8a8 -- Resolve "Update to Angular 11"
|/  
*   2f94029 -- Merge branch '43-recreate-package-lock-for-new-package-registry-muni-kypo-crp-frontend-angular-models-kypo' into 'master'
|\  
| * b8c9c07 -- reacreate package lock
|/  
*   6db319a -- Merge branch '42-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 7fe3c8b -- Resolve "Migrate from tslint to eslint"
|/  
* af94615 -- Update project package.json version based on GitLab tag. Done by CI
*   105c759 -- Merge branch '41-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * eebd0de -- rename package scope and update dependencies
|/  
* 5f4642b -- Update project package.json version based on GitLab tag. Done by CI
*   6d4d5ea -- Merge branch '40-update-dependencies-to-new-format' into 'master'
|\  
| * a97e66c -- Resolve "Update dependencies to new format"
|/  
* 3fe51b8 -- Update project package.json version based on GitLab tag. Done by CI
*   670bd06 -- Merge branch '39-rename-package-to-kypo-topology-graph' into 'master'
|\  
| * f22bbe7 -- Resolve "Rename package to @kypo/topology-graph"
|/  
*   973f029 -- Merge branch '38-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * fa1b651 -- Remove personal info from README
|/  
*   aae734f -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 41bbf78 -- Resolve "Use Cypress image in CI"
|/  
* ae4f343 -- Update project package.json version based on GitLab tag. Done by CI
*   19a56f6 -- Merge branch '36-update-to-angualar-10' into 'master'
|\  
| * 4d6a53f -- Resolve "Update to Angular 10"
|/  
* bb653af -- Update project package.json version based on GitLab tag. Done by CI
### 14.0.2 Rename from kypo2 to kypo.
* 45780fd -- [CI/CD] Update packages.json version based on GitLab tag.
*   0d70187 -- Merge branch '65-rename-from-kypo2-to-kypo' into 'master'
|\  
| * 8e8d954 -- Resolve "Rename from kypo2 to kypo"
|/  
* d4b6392 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2276695 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ba17f3c -- Merge branch '64-fix-imports' into 'master'
|\  
| * 6d25f0e -- Resolve "Fix imports"
|/  
* 347f05e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 130df10 -- [CI/CD] Update packages.json version based on GitLab tag.
*   89e4c19 -- Merge branch '63-update-to-angular-14' into 'master'
|\  
| * 51310b6 -- Resolve "Update to Angular 14"
|/  
*   ac8272f -- Merge branch '62-fix-context-menu-clipping' into 'master'
|\  
| * c5ae6b4 -- Resolve "Fix context menu clipping"
|/  
* aedd30f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* ae7bb99 -- [CI/CD] Update packages.json version based on GitLab tag.
*   452a279 -- Merge branch 'fix-context-menu' into 'master'
|\  
| * 665d6ac -- Fix business logic of context menu
|/  
* 3cc2eec -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 16ecabb -- [CI/CD] Update packages.json version based on GitLab tag.
*   723cfad -- Merge branch '61-review-open-console-options-and-remove-resume-option' into 'master'
|\  
| * eec6e04 -- Removed Resume option from node menu. Renamed open console options.
|/  
* 16aa303 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f6e87de -- [CI/CD] Update packages.json version based on GitLab tag.
*   3e0770d -- Merge branch '60-update-to-angular-13' into 'master'
|\  
| * 08e1839 -- Resolve "Update to Angular 13"
|/  
*   9a43908 -- Merge branch '59-display-the-topology-based-on-the-sandbox-definition-id' into 'master'
|\  
| * 391a95f -- Resolve "Display the topology based on the sandbox definition ID"
|/  
* 4d6b41a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fe31cfd -- [CI/CD] Update packages.json version based on GitLab tag.
*   3faed73 -- Merge branch '58-bump-version' into 'master'
|\  
| * 292abe6 -- Bump version
|/  
*   8410e87 -- Merge branch '57-fix-auto-topology-expansion-with-small-number-of-nodes' into 'master'
|\  
| * 59e0050 -- Move topology expansion decision to visuals
|/  
* b1c971c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* edf8c00 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7773b1c -- Merge branch '56-bump-version-of-d3' into 'master'
|\  
| * e5e7fb2 -- Resolve "Bump version of d3"
|/  
* ba84d32 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 52c67ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   e3a5ddf -- Merge branch '55-add-attributes-gui_access-and-os_type-to-routerdto' into 'master'
|\  
| * 57823ce -- Add missing attributes. Modified mapper accordingly.
|/  
* 37d0f58 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b31153f -- [CI/CD] Update packages.json version based on GitLab tag.
*   ecbd2f5 -- Merge branch '53-dispaly-open-cli-and-open-gui-option-based-on-the-os-type-and-guiaccess-attribute-of-the-host' into 'master'
|\  
| * bd8032c -- Resolve "Dispaly Open CLI and Open GUI option based on the OS type and guiAccess attribute of the host"
|/  
*   d683ace -- Merge branch '54-add-console-preload' into 'master'
|\  
| * 69439f1 -- Resolve "Add console preload"
|/  
*   b740a39 -- Merge branch '51-switching-the-host-console-access-to-a-single-phase' into 'master'
|\  
| * 3d4a8f1 -- Remove loading of spice console
|/  
* 9b52172 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 340fa12 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e42f178 -- Merge branch '50-add-simple-legend-for-topology-nodes' into 'master'
|\  
| * 344b252 -- Resolve "Add simple legend for topology nodes"
|/  
*   cf7e3b1 -- Merge branch '52-add-license-file' into 'master'
|\  
| * 2fd2ad6 -- Add license file
|/  
*   1e4a62f -- Merge branch '49-update-the-look-of-topology-nodes' into 'master'
|\  
| * 00fad21 -- Update nodes visibility and icons
|/  
* a9aa75b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 73354b2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   34da99c -- Merge branch '48-add-option-to-open-console-using-apache-guacamole' into 'master'
|\  
| * 916ef3a -- Resolve "Add option to open console using apache guacamole"
|/  
* b8b6b33 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 70fd3a9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8a59576 -- Merge branch '47-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * b6a4002 -- Update gitlab CI
|/  
* a291334 -- Update project package.json version based on GitLab tag. Done by CI
*   65ea149 -- Merge branch '46-update-to-angular-12' into 'master'
|\  
| * cbe2590 -- Resolve "Update to Angular 12"
|/  
* 9e6fcf5 -- Update project package.json version based on GitLab tag. Done by CI
*   4654845 -- Merge branch '45-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0aeb4a5 -- Update peerDependencies and dependencies
|/  
*   ab1fdb6 -- Merge branch '44-update-to-angular-11' into 'master'
|\  
| * 90fe8a8 -- Resolve "Update to Angular 11"
|/  
*   2f94029 -- Merge branch '43-recreate-package-lock-for-new-package-registry-muni-kypo-crp-frontend-angular-models-kypo' into 'master'
|\  
| * b8c9c07 -- reacreate package lock
|/  
*   6db319a -- Merge branch '42-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 7fe3c8b -- Resolve "Migrate from tslint to eslint"
|/  
* af94615 -- Update project package.json version based on GitLab tag. Done by CI
*   105c759 -- Merge branch '41-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * eebd0de -- rename package scope and update dependencies
|/  
* 5f4642b -- Update project package.json version based on GitLab tag. Done by CI
*   6d4d5ea -- Merge branch '40-update-dependencies-to-new-format' into 'master'
|\  
| * a97e66c -- Resolve "Update dependencies to new format"
|/  
* 3fe51b8 -- Update project package.json version based on GitLab tag. Done by CI
*   670bd06 -- Merge branch '39-rename-package-to-kypo-topology-graph' into 'master'
|\  
| * f22bbe7 -- Resolve "Rename package to @kypo/topology-graph"
|/  
*   973f029 -- Merge branch '38-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * fa1b651 -- Remove personal info from README
|/  
*   aae734f -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 41bbf78 -- Resolve "Use Cypress image in CI"
|/  
* ae4f343 -- Update project package.json version based on GitLab tag. Done by CI
*   19a56f6 -- Merge branch '36-update-to-angualar-10' into 'master'
|\  
| * 4d6a53f -- Resolve "Update to Angular 10"
|/  
* bb653af -- Update project package.json version based on GitLab tag. Done by CI
### 14.0.1 Fix imports.
* 2276695 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ba17f3c -- Merge branch '64-fix-imports' into 'master'
|\  
| * 6d25f0e -- Resolve "Fix imports"
|/  
* 347f05e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 130df10 -- [CI/CD] Update packages.json version based on GitLab tag.
*   89e4c19 -- Merge branch '63-update-to-angular-14' into 'master'
|\  
| * 51310b6 -- Resolve "Update to Angular 14"
|/  
*   ac8272f -- Merge branch '62-fix-context-menu-clipping' into 'master'
|\  
| * c5ae6b4 -- Resolve "Fix context menu clipping"
|/  
* aedd30f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* ae7bb99 -- [CI/CD] Update packages.json version based on GitLab tag.
*   452a279 -- Merge branch 'fix-context-menu' into 'master'
|\  
| * 665d6ac -- Fix business logic of context menu
|/  
* 3cc2eec -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 16ecabb -- [CI/CD] Update packages.json version based on GitLab tag.
*   723cfad -- Merge branch '61-review-open-console-options-and-remove-resume-option' into 'master'
|\  
| * eec6e04 -- Removed Resume option from node menu. Renamed open console options.
|/  
* 16aa303 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f6e87de -- [CI/CD] Update packages.json version based on GitLab tag.
*   3e0770d -- Merge branch '60-update-to-angular-13' into 'master'
|\  
| * 08e1839 -- Resolve "Update to Angular 13"
|/  
*   9a43908 -- Merge branch '59-display-the-topology-based-on-the-sandbox-definition-id' into 'master'
|\  
| * 391a95f -- Resolve "Display the topology based on the sandbox definition ID"
|/  
* 4d6b41a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fe31cfd -- [CI/CD] Update packages.json version based on GitLab tag.
*   3faed73 -- Merge branch '58-bump-version' into 'master'
|\  
| * 292abe6 -- Bump version
|/  
*   8410e87 -- Merge branch '57-fix-auto-topology-expansion-with-small-number-of-nodes' into 'master'
|\  
| * 59e0050 -- Move topology expansion decision to visuals
|/  
* b1c971c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* edf8c00 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7773b1c -- Merge branch '56-bump-version-of-d3' into 'master'
|\  
| * e5e7fb2 -- Resolve "Bump version of d3"
|/  
* ba84d32 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 52c67ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   e3a5ddf -- Merge branch '55-add-attributes-gui_access-and-os_type-to-routerdto' into 'master'
|\  
| * 57823ce -- Add missing attributes. Modified mapper accordingly.
|/  
* 37d0f58 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b31153f -- [CI/CD] Update packages.json version based on GitLab tag.
*   ecbd2f5 -- Merge branch '53-dispaly-open-cli-and-open-gui-option-based-on-the-os-type-and-guiaccess-attribute-of-the-host' into 'master'
|\  
| * bd8032c -- Resolve "Dispaly Open CLI and Open GUI option based on the OS type and guiAccess attribute of the host"
|/  
*   d683ace -- Merge branch '54-add-console-preload' into 'master'
|\  
| * 69439f1 -- Resolve "Add console preload"
|/  
*   b740a39 -- Merge branch '51-switching-the-host-console-access-to-a-single-phase' into 'master'
|\  
| * 3d4a8f1 -- Remove loading of spice console
|/  
* 9b52172 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 340fa12 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e42f178 -- Merge branch '50-add-simple-legend-for-topology-nodes' into 'master'
|\  
| * 344b252 -- Resolve "Add simple legend for topology nodes"
|/  
*   cf7e3b1 -- Merge branch '52-add-license-file' into 'master'
|\  
| * 2fd2ad6 -- Add license file
|/  
*   1e4a62f -- Merge branch '49-update-the-look-of-topology-nodes' into 'master'
|\  
| * 00fad21 -- Update nodes visibility and icons
|/  
* a9aa75b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 73354b2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   34da99c -- Merge branch '48-add-option-to-open-console-using-apache-guacamole' into 'master'
|\  
| * 916ef3a -- Resolve "Add option to open console using apache guacamole"
|/  
* b8b6b33 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 70fd3a9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8a59576 -- Merge branch '47-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * b6a4002 -- Update gitlab CI
|/  
* a291334 -- Update project package.json version based on GitLab tag. Done by CI
*   65ea149 -- Merge branch '46-update-to-angular-12' into 'master'
|\  
| * cbe2590 -- Resolve "Update to Angular 12"
|/  
* 9e6fcf5 -- Update project package.json version based on GitLab tag. Done by CI
*   4654845 -- Merge branch '45-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0aeb4a5 -- Update peerDependencies and dependencies
|/  
*   ab1fdb6 -- Merge branch '44-update-to-angular-11' into 'master'
|\  
| * 90fe8a8 -- Resolve "Update to Angular 11"
|/  
*   2f94029 -- Merge branch '43-recreate-package-lock-for-new-package-registry-muni-kypo-crp-frontend-angular-models-kypo' into 'master'
|\  
| * b8c9c07 -- reacreate package lock
|/  
*   6db319a -- Merge branch '42-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 7fe3c8b -- Resolve "Migrate from tslint to eslint"
|/  
* af94615 -- Update project package.json version based on GitLab tag. Done by CI
*   105c759 -- Merge branch '41-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * eebd0de -- rename package scope and update dependencies
|/  
* 5f4642b -- Update project package.json version based on GitLab tag. Done by CI
*   6d4d5ea -- Merge branch '40-update-dependencies-to-new-format' into 'master'
|\  
| * a97e66c -- Resolve "Update dependencies to new format"
|/  
* 3fe51b8 -- Update project package.json version based on GitLab tag. Done by CI
*   670bd06 -- Merge branch '39-rename-package-to-kypo-topology-graph' into 'master'
|\  
| * f22bbe7 -- Resolve "Rename package to @kypo/topology-graph"
|/  
*   973f029 -- Merge branch '38-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * fa1b651 -- Remove personal info from README
|/  
*   aae734f -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 41bbf78 -- Resolve "Use Cypress image in CI"
|/  
* ae4f343 -- Update project package.json version based on GitLab tag. Done by CI
*   19a56f6 -- Merge branch '36-update-to-angualar-10' into 'master'
|\  
| * 4d6a53f -- Resolve "Update to Angular 10"
|/  
* bb653af -- Update project package.json version based on GitLab tag. Done by CI
### 14.0.0 Update to Angular 14 and fix for topology menu being rendered outside the topology.
* 130df10 -- [CI/CD] Update packages.json version based on GitLab tag.
*   89e4c19 -- Merge branch '63-update-to-angular-14' into 'master'
|\  
| * 51310b6 -- Resolve "Update to Angular 14"
|/  
*   ac8272f -- Merge branch '62-fix-context-menu-clipping' into 'master'
|\  
| * c5ae6b4 -- Resolve "Fix context menu clipping"
|/  
* aedd30f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* ae7bb99 -- [CI/CD] Update packages.json version based on GitLab tag.
*   452a279 -- Merge branch 'fix-context-menu' into 'master'
|\  
| * 665d6ac -- Fix business logic of context menu
|/  
* 3cc2eec -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 16ecabb -- [CI/CD] Update packages.json version based on GitLab tag.
*   723cfad -- Merge branch '61-review-open-console-options-and-remove-resume-option' into 'master'
|\  
| * eec6e04 -- Removed Resume option from node menu. Renamed open console options.
|/  
* 16aa303 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f6e87de -- [CI/CD] Update packages.json version based on GitLab tag.
*   3e0770d -- Merge branch '60-update-to-angular-13' into 'master'
|\  
| * 08e1839 -- Resolve "Update to Angular 13"
|/  
*   9a43908 -- Merge branch '59-display-the-topology-based-on-the-sandbox-definition-id' into 'master'
|\  
| * 391a95f -- Resolve "Display the topology based on the sandbox definition ID"
|/  
* 4d6b41a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fe31cfd -- [CI/CD] Update packages.json version based on GitLab tag.
*   3faed73 -- Merge branch '58-bump-version' into 'master'
|\  
| * 292abe6 -- Bump version
|/  
*   8410e87 -- Merge branch '57-fix-auto-topology-expansion-with-small-number-of-nodes' into 'master'
|\  
| * 59e0050 -- Move topology expansion decision to visuals
|/  
* b1c971c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* edf8c00 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7773b1c -- Merge branch '56-bump-version-of-d3' into 'master'
|\  
| * e5e7fb2 -- Resolve "Bump version of d3"
|/  
* ba84d32 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 52c67ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   e3a5ddf -- Merge branch '55-add-attributes-gui_access-and-os_type-to-routerdto' into 'master'
|\  
| * 57823ce -- Add missing attributes. Modified mapper accordingly.
|/  
* 37d0f58 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b31153f -- [CI/CD] Update packages.json version based on GitLab tag.
*   ecbd2f5 -- Merge branch '53-dispaly-open-cli-and-open-gui-option-based-on-the-os-type-and-guiaccess-attribute-of-the-host' into 'master'
|\  
| * bd8032c -- Resolve "Dispaly Open CLI and Open GUI option based on the OS type and guiAccess attribute of the host"
|/  
*   d683ace -- Merge branch '54-add-console-preload' into 'master'
|\  
| * 69439f1 -- Resolve "Add console preload"
|/  
*   b740a39 -- Merge branch '51-switching-the-host-console-access-to-a-single-phase' into 'master'
|\  
| * 3d4a8f1 -- Remove loading of spice console
|/  
* 9b52172 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 340fa12 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e42f178 -- Merge branch '50-add-simple-legend-for-topology-nodes' into 'master'
|\  
| * 344b252 -- Resolve "Add simple legend for topology nodes"
|/  
*   cf7e3b1 -- Merge branch '52-add-license-file' into 'master'
|\  
| * 2fd2ad6 -- Add license file
|/  
*   1e4a62f -- Merge branch '49-update-the-look-of-topology-nodes' into 'master'
|\  
| * 00fad21 -- Update nodes visibility and icons
|/  
* a9aa75b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 73354b2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   34da99c -- Merge branch '48-add-option-to-open-console-using-apache-guacamole' into 'master'
|\  
| * 916ef3a -- Resolve "Add option to open console using apache guacamole"
|/  
* b8b6b33 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 70fd3a9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8a59576 -- Merge branch '47-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * b6a4002 -- Update gitlab CI
|/  
* a291334 -- Update project package.json version based on GitLab tag. Done by CI
*   65ea149 -- Merge branch '46-update-to-angular-12' into 'master'
|\  
| * cbe2590 -- Resolve "Update to Angular 12"
|/  
* 9e6fcf5 -- Update project package.json version based on GitLab tag. Done by CI
*   4654845 -- Merge branch '45-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0aeb4a5 -- Update peerDependencies and dependencies
|/  
*   ab1fdb6 -- Merge branch '44-update-to-angular-11' into 'master'
|\  
| * 90fe8a8 -- Resolve "Update to Angular 11"
|/  
*   2f94029 -- Merge branch '43-recreate-package-lock-for-new-package-registry-muni-kypo-crp-frontend-angular-models-kypo' into 'master'
|\  
| * b8c9c07 -- reacreate package lock
|/  
*   6db319a -- Merge branch '42-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 7fe3c8b -- Resolve "Migrate from tslint to eslint"
|/  
* af94615 -- Update project package.json version based on GitLab tag. Done by CI
*   105c759 -- Merge branch '41-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * eebd0de -- rename package scope and update dependencies
|/  
* 5f4642b -- Update project package.json version based on GitLab tag. Done by CI
*   6d4d5ea -- Merge branch '40-update-dependencies-to-new-format' into 'master'
|\  
| * a97e66c -- Resolve "Update dependencies to new format"
|/  
* 3fe51b8 -- Update project package.json version based on GitLab tag. Done by CI
*   670bd06 -- Merge branch '39-rename-package-to-kypo-topology-graph' into 'master'
|\  
| * f22bbe7 -- Resolve "Rename package to @kypo/topology-graph"
|/  
*   973f029 -- Merge branch '38-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * fa1b651 -- Remove personal info from README
|/  
*   aae734f -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 41bbf78 -- Resolve "Use Cypress image in CI"
|/  
* ae4f343 -- Update project package.json version based on GitLab tag. Done by CI
*   19a56f6 -- Merge branch '36-update-to-angualar-10' into 'master'
|\  
| * 4d6a53f -- Resolve "Update to Angular 10"
|/  
* bb653af -- Update project package.json version based on GitLab tag. Done by CI
### 13.0.2 Fix bug that was loading spice console instead of guacamole console.
* ae7bb99 -- [CI/CD] Update packages.json version based on GitLab tag.
*   452a279 -- Merge branch 'fix-context-menu' into 'master'
|\  
| * 665d6ac -- Fix business logic of context menu
|/  
* 3cc2eec -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 16ecabb -- [CI/CD] Update packages.json version based on GitLab tag.
*   723cfad -- Merge branch '61-review-open-console-options-and-remove-resume-option' into 'master'
|\  
| * eec6e04 -- Removed Resume option from node menu. Renamed open console options.
|/  
* 16aa303 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f6e87de -- [CI/CD] Update packages.json version based on GitLab tag.
*   3e0770d -- Merge branch '60-update-to-angular-13' into 'master'
|\  
| * 08e1839 -- Resolve "Update to Angular 13"
|/  
*   9a43908 -- Merge branch '59-display-the-topology-based-on-the-sandbox-definition-id' into 'master'
|\  
| * 391a95f -- Resolve "Display the topology based on the sandbox definition ID"
|/  
* 4d6b41a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fe31cfd -- [CI/CD] Update packages.json version based on GitLab tag.
*   3faed73 -- Merge branch '58-bump-version' into 'master'
|\  
| * 292abe6 -- Bump version
|/  
*   8410e87 -- Merge branch '57-fix-auto-topology-expansion-with-small-number-of-nodes' into 'master'
|\  
| * 59e0050 -- Move topology expansion decision to visuals
|/  
* b1c971c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* edf8c00 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7773b1c -- Merge branch '56-bump-version-of-d3' into 'master'
|\  
| * e5e7fb2 -- Resolve "Bump version of d3"
|/  
* ba84d32 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 52c67ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   e3a5ddf -- Merge branch '55-add-attributes-gui_access-and-os_type-to-routerdto' into 'master'
|\  
| * 57823ce -- Add missing attributes. Modified mapper accordingly.
|/  
* 37d0f58 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b31153f -- [CI/CD] Update packages.json version based on GitLab tag.
*   ecbd2f5 -- Merge branch '53-dispaly-open-cli-and-open-gui-option-based-on-the-os-type-and-guiaccess-attribute-of-the-host' into 'master'
|\  
| * bd8032c -- Resolve "Dispaly Open CLI and Open GUI option based on the OS type and guiAccess attribute of the host"
|/  
*   d683ace -- Merge branch '54-add-console-preload' into 'master'
|\  
| * 69439f1 -- Resolve "Add console preload"
|/  
*   b740a39 -- Merge branch '51-switching-the-host-console-access-to-a-single-phase' into 'master'
|\  
| * 3d4a8f1 -- Remove loading of spice console
|/  
* 9b52172 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 340fa12 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e42f178 -- Merge branch '50-add-simple-legend-for-topology-nodes' into 'master'
|\  
| * 344b252 -- Resolve "Add simple legend for topology nodes"
|/  
*   cf7e3b1 -- Merge branch '52-add-license-file' into 'master'
|\  
| * 2fd2ad6 -- Add license file
|/  
*   1e4a62f -- Merge branch '49-update-the-look-of-topology-nodes' into 'master'
|\  
| * 00fad21 -- Update nodes visibility and icons
|/  
* a9aa75b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 73354b2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   34da99c -- Merge branch '48-add-option-to-open-console-using-apache-guacamole' into 'master'
|\  
| * 916ef3a -- Resolve "Add option to open console using apache guacamole"
|/  
* b8b6b33 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 70fd3a9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8a59576 -- Merge branch '47-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * b6a4002 -- Update gitlab CI
|/  
* a291334 -- Update project package.json version based on GitLab tag. Done by CI
*   65ea149 -- Merge branch '46-update-to-angular-12' into 'master'
|\  
| * cbe2590 -- Resolve "Update to Angular 12"
|/  
* 9e6fcf5 -- Update project package.json version based on GitLab tag. Done by CI
*   4654845 -- Merge branch '45-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0aeb4a5 -- Update peerDependencies and dependencies
|/  
*   ab1fdb6 -- Merge branch '44-update-to-angular-11' into 'master'
|\  
| * 90fe8a8 -- Resolve "Update to Angular 11"
|/  
*   2f94029 -- Merge branch '43-recreate-package-lock-for-new-package-registry-muni-kypo-crp-frontend-angular-models-kypo' into 'master'
|\  
| * b8c9c07 -- reacreate package lock
|/  
*   6db319a -- Merge branch '42-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 7fe3c8b -- Resolve "Migrate from tslint to eslint"
|/  
* af94615 -- Update project package.json version based on GitLab tag. Done by CI
*   105c759 -- Merge branch '41-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * eebd0de -- rename package scope and update dependencies
|/  
* 5f4642b -- Update project package.json version based on GitLab tag. Done by CI
*   6d4d5ea -- Merge branch '40-update-dependencies-to-new-format' into 'master'
|\  
| * a97e66c -- Resolve "Update dependencies to new format"
|/  
* 3fe51b8 -- Update project package.json version based on GitLab tag. Done by CI
*   670bd06 -- Merge branch '39-rename-package-to-kypo-topology-graph' into 'master'
|\  
| * f22bbe7 -- Resolve "Rename package to @kypo/topology-graph"
|/  
*   973f029 -- Merge branch '38-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * fa1b651 -- Remove personal info from README
|/  
*   aae734f -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 41bbf78 -- Resolve "Use Cypress image in CI"
|/  
* ae4f343 -- Update project package.json version based on GitLab tag. Done by CI
*   19a56f6 -- Merge branch '36-update-to-angualar-10' into 'master'
|\  
| * 4d6a53f -- Resolve "Update to Angular 10"
|/  
* bb653af -- Update project package.json version based on GitLab tag. Done by CI
### 13.0.1 Removed Resume option from node menu. Renamed open console options.
* 16ecabb -- [CI/CD] Update packages.json version based on GitLab tag.
*   723cfad -- Merge branch '61-review-open-console-options-and-remove-resume-option' into 'master'
|\  
| * eec6e04 -- Removed Resume option from node menu. Renamed open console options.
|/  
* 16aa303 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f6e87de -- [CI/CD] Update packages.json version based on GitLab tag.
*   3e0770d -- Merge branch '60-update-to-angular-13' into 'master'
|\  
| * 08e1839 -- Resolve "Update to Angular 13"
|/  
*   9a43908 -- Merge branch '59-display-the-topology-based-on-the-sandbox-definition-id' into 'master'
|\  
| * 391a95f -- Resolve "Display the topology based on the sandbox definition ID"
|/  
* 4d6b41a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fe31cfd -- [CI/CD] Update packages.json version based on GitLab tag.
*   3faed73 -- Merge branch '58-bump-version' into 'master'
|\  
| * 292abe6 -- Bump version
|/  
*   8410e87 -- Merge branch '57-fix-auto-topology-expansion-with-small-number-of-nodes' into 'master'
|\  
| * 59e0050 -- Move topology expansion decision to visuals
|/  
* b1c971c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* edf8c00 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7773b1c -- Merge branch '56-bump-version-of-d3' into 'master'
|\  
| * e5e7fb2 -- Resolve "Bump version of d3"
|/  
* ba84d32 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 52c67ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   e3a5ddf -- Merge branch '55-add-attributes-gui_access-and-os_type-to-routerdto' into 'master'
|\  
| * 57823ce -- Add missing attributes. Modified mapper accordingly.
|/  
* 37d0f58 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b31153f -- [CI/CD] Update packages.json version based on GitLab tag.
*   ecbd2f5 -- Merge branch '53-dispaly-open-cli-and-open-gui-option-based-on-the-os-type-and-guiaccess-attribute-of-the-host' into 'master'
|\  
| * bd8032c -- Resolve "Dispaly Open CLI and Open GUI option based on the OS type and guiAccess attribute of the host"
|/  
*   d683ace -- Merge branch '54-add-console-preload' into 'master'
|\  
| * 69439f1 -- Resolve "Add console preload"
|/  
*   b740a39 -- Merge branch '51-switching-the-host-console-access-to-a-single-phase' into 'master'
|\  
| * 3d4a8f1 -- Remove loading of spice console
|/  
* 9b52172 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 340fa12 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e42f178 -- Merge branch '50-add-simple-legend-for-topology-nodes' into 'master'
|\  
| * 344b252 -- Resolve "Add simple legend for topology nodes"
|/  
*   cf7e3b1 -- Merge branch '52-add-license-file' into 'master'
|\  
| * 2fd2ad6 -- Add license file
|/  
*   1e4a62f -- Merge branch '49-update-the-look-of-topology-nodes' into 'master'
|\  
| * 00fad21 -- Update nodes visibility and icons
|/  
* a9aa75b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 73354b2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   34da99c -- Merge branch '48-add-option-to-open-console-using-apache-guacamole' into 'master'
|\  
| * 916ef3a -- Resolve "Add option to open console using apache guacamole"
|/  
* b8b6b33 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 70fd3a9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8a59576 -- Merge branch '47-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * b6a4002 -- Update gitlab CI
|/  
* a291334 -- Update project package.json version based on GitLab tag. Done by CI
*   65ea149 -- Merge branch '46-update-to-angular-12' into 'master'
|\  
| * cbe2590 -- Resolve "Update to Angular 12"
|/  
* 9e6fcf5 -- Update project package.json version based on GitLab tag. Done by CI
*   4654845 -- Merge branch '45-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0aeb4a5 -- Update peerDependencies and dependencies
|/  
*   ab1fdb6 -- Merge branch '44-update-to-angular-11' into 'master'
|\  
| * 90fe8a8 -- Resolve "Update to Angular 11"
|/  
*   2f94029 -- Merge branch '43-recreate-package-lock-for-new-package-registry-muni-kypo-crp-frontend-angular-models-kypo' into 'master'
|\  
| * b8c9c07 -- reacreate package lock
|/  
*   6db319a -- Merge branch '42-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 7fe3c8b -- Resolve "Migrate from tslint to eslint"
|/  
* af94615 -- Update project package.json version based on GitLab tag. Done by CI
*   105c759 -- Merge branch '41-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * eebd0de -- rename package scope and update dependencies
|/  
* 5f4642b -- Update project package.json version based on GitLab tag. Done by CI
*   6d4d5ea -- Merge branch '40-update-dependencies-to-new-format' into 'master'
|\  
| * a97e66c -- Resolve "Update dependencies to new format"
|/  
* 3fe51b8 -- Update project package.json version based on GitLab tag. Done by CI
*   670bd06 -- Merge branch '39-rename-package-to-kypo-topology-graph' into 'master'
|\  
| * f22bbe7 -- Resolve "Rename package to @kypo/topology-graph"
|/  
*   973f029 -- Merge branch '38-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * fa1b651 -- Remove personal info from README
|/  
*   aae734f -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 41bbf78 -- Resolve "Use Cypress image in CI"
|/  
* ae4f343 -- Update project package.json version based on GitLab tag. Done by CI
*   19a56f6 -- Merge branch '36-update-to-angualar-10' into 'master'
|\  
| * 4d6a53f -- Resolve "Update to Angular 10"
|/  
* bb653af -- Update project package.json version based on GitLab tag. Done by CI
### 13.0.0 Update to Angular 13, CI/CD optimization, topology displayed based on sandbox definition id.
* f6e87de -- [CI/CD] Update packages.json version based on GitLab tag.
*   3e0770d -- Merge branch '60-update-to-angular-13' into 'master'
|\  
| * 08e1839 -- Resolve "Update to Angular 13"
|/  
*   9a43908 -- Merge branch '59-display-the-topology-based-on-the-sandbox-definition-id' into 'master'
|\  
| * 391a95f -- Resolve "Display the topology based on the sandbox definition ID"
|/  
* 4d6b41a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fe31cfd -- [CI/CD] Update packages.json version based on GitLab tag.
*   3faed73 -- Merge branch '58-bump-version' into 'master'
|\  
| * 292abe6 -- Bump version
|/  
*   8410e87 -- Merge branch '57-fix-auto-topology-expansion-with-small-number-of-nodes' into 'master'
|\  
| * 59e0050 -- Move topology expansion decision to visuals
|/  
* b1c971c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* edf8c00 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7773b1c -- Merge branch '56-bump-version-of-d3' into 'master'
|\  
| * e5e7fb2 -- Resolve "Bump version of d3"
|/  
* ba84d32 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 52c67ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   e3a5ddf -- Merge branch '55-add-attributes-gui_access-and-os_type-to-routerdto' into 'master'
|\  
| * 57823ce -- Add missing attributes. Modified mapper accordingly.
|/  
* 37d0f58 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b31153f -- [CI/CD] Update packages.json version based on GitLab tag.
*   ecbd2f5 -- Merge branch '53-dispaly-open-cli-and-open-gui-option-based-on-the-os-type-and-guiaccess-attribute-of-the-host' into 'master'
|\  
| * bd8032c -- Resolve "Dispaly Open CLI and Open GUI option based on the OS type and guiAccess attribute of the host"
|/  
*   d683ace -- Merge branch '54-add-console-preload' into 'master'
|\  
| * 69439f1 -- Resolve "Add console preload"
|/  
*   b740a39 -- Merge branch '51-switching-the-host-console-access-to-a-single-phase' into 'master'
|\  
| * 3d4a8f1 -- Remove loading of spice console
|/  
* 9b52172 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 340fa12 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e42f178 -- Merge branch '50-add-simple-legend-for-topology-nodes' into 'master'
|\  
| * 344b252 -- Resolve "Add simple legend for topology nodes"
|/  
*   cf7e3b1 -- Merge branch '52-add-license-file' into 'master'
|\  
| * 2fd2ad6 -- Add license file
|/  
*   1e4a62f -- Merge branch '49-update-the-look-of-topology-nodes' into 'master'
|\  
| * 00fad21 -- Update nodes visibility and icons
|/  
* a9aa75b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 73354b2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   34da99c -- Merge branch '48-add-option-to-open-console-using-apache-guacamole' into 'master'
|\  
| * 916ef3a -- Resolve "Add option to open console using apache guacamole"
|/  
* b8b6b33 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 70fd3a9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8a59576 -- Merge branch '47-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * b6a4002 -- Update gitlab CI
|/  
* a291334 -- Update project package.json version based on GitLab tag. Done by CI
*   65ea149 -- Merge branch '46-update-to-angular-12' into 'master'
|\  
| * cbe2590 -- Resolve "Update to Angular 12"
|/  
* 9e6fcf5 -- Update project package.json version based on GitLab tag. Done by CI
*   4654845 -- Merge branch '45-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0aeb4a5 -- Update peerDependencies and dependencies
|/  
*   ab1fdb6 -- Merge branch '44-update-to-angular-11' into 'master'
|\  
| * 90fe8a8 -- Resolve "Update to Angular 11"
|/  
*   2f94029 -- Merge branch '43-recreate-package-lock-for-new-package-registry-muni-kypo-crp-frontend-angular-models-kypo' into 'master'
|\  
| * b8c9c07 -- reacreate package lock
|/  
*   6db319a -- Merge branch '42-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 7fe3c8b -- Resolve "Migrate from tslint to eslint"
|/  
* af94615 -- Update project package.json version based on GitLab tag. Done by CI
*   105c759 -- Merge branch '41-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * eebd0de -- rename package scope and update dependencies
|/  
* 5f4642b -- Update project package.json version based on GitLab tag. Done by CI
*   6d4d5ea -- Merge branch '40-update-dependencies-to-new-format' into 'master'
|\  
| * a97e66c -- Resolve "Update dependencies to new format"
|/  
* 3fe51b8 -- Update project package.json version based on GitLab tag. Done by CI
*   670bd06 -- Merge branch '39-rename-package-to-kypo-topology-graph' into 'master'
|\  
| * f22bbe7 -- Resolve "Rename package to @kypo/topology-graph"
|/  
*   973f029 -- Merge branch '38-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * fa1b651 -- Remove personal info from README
|/  
*   aae734f -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 41bbf78 -- Resolve "Use Cypress image in CI"
|/  
* ae4f343 -- Update project package.json version based on GitLab tag. Done by CI
*   19a56f6 -- Merge branch '36-update-to-angualar-10' into 'master'
|\  
| * 4d6a53f -- Resolve "Update to Angular 10"
|/  
* bb653af -- Update project package.json version based on GitLab tag. Done by CI
### 12.1.3 Add auto expand for nodes
* fe31cfd -- [CI/CD] Update packages.json version based on GitLab tag.
*   3faed73 -- Merge branch '58-bump-version' into 'master'
|\  
| * 292abe6 -- Bump version
|/  
*   8410e87 -- Merge branch '57-fix-auto-topology-expansion-with-small-number-of-nodes' into 'master'
|\  
| * 59e0050 -- Move topology expansion decision to visuals
|/  
* b1c971c -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* edf8c00 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7773b1c -- Merge branch '56-bump-version-of-d3' into 'master'
|\  
| * e5e7fb2 -- Resolve "Bump version of d3"
|/  
* ba84d32 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 52c67ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   e3a5ddf -- Merge branch '55-add-attributes-gui_access-and-os_type-to-routerdto' into 'master'
|\  
| * 57823ce -- Add missing attributes. Modified mapper accordingly.
|/  
* 37d0f58 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b31153f -- [CI/CD] Update packages.json version based on GitLab tag.
*   ecbd2f5 -- Merge branch '53-dispaly-open-cli-and-open-gui-option-based-on-the-os-type-and-guiaccess-attribute-of-the-host' into 'master'
|\  
| * bd8032c -- Resolve "Dispaly Open CLI and Open GUI option based on the OS type and guiAccess attribute of the host"
|/  
*   d683ace -- Merge branch '54-add-console-preload' into 'master'
|\  
| * 69439f1 -- Resolve "Add console preload"
|/  
*   b740a39 -- Merge branch '51-switching-the-host-console-access-to-a-single-phase' into 'master'
|\  
| * 3d4a8f1 -- Remove loading of spice console
|/  
* 9b52172 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 340fa12 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e42f178 -- Merge branch '50-add-simple-legend-for-topology-nodes' into 'master'
|\  
| * 344b252 -- Resolve "Add simple legend for topology nodes"
|/  
*   cf7e3b1 -- Merge branch '52-add-license-file' into 'master'
|\  
| * 2fd2ad6 -- Add license file
|/  
*   1e4a62f -- Merge branch '49-update-the-look-of-topology-nodes' into 'master'
|\  
| * 00fad21 -- Update nodes visibility and icons
|/  
* a9aa75b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 73354b2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   34da99c -- Merge branch '48-add-option-to-open-console-using-apache-guacamole' into 'master'
|\  
| * 916ef3a -- Resolve "Add option to open console using apache guacamole"
|/  
* b8b6b33 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 70fd3a9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8a59576 -- Merge branch '47-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * b6a4002 -- Update gitlab CI
|/  
* a291334 -- Update project package.json version based on GitLab tag. Done by CI
*   65ea149 -- Merge branch '46-update-to-angular-12' into 'master'
|\  
| * cbe2590 -- Resolve "Update to Angular 12"
|/  
* 9e6fcf5 -- Update project package.json version based on GitLab tag. Done by CI
*   4654845 -- Merge branch '45-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0aeb4a5 -- Update peerDependencies and dependencies
|/  
*   ab1fdb6 -- Merge branch '44-update-to-angular-11' into 'master'
|\  
| * 90fe8a8 -- Resolve "Update to Angular 11"
|/  
*   2f94029 -- Merge branch '43-recreate-package-lock-for-new-package-registry-muni-kypo-crp-frontend-angular-models-kypo' into 'master'
|\  
| * b8c9c07 -- reacreate package lock
|/  
*   6db319a -- Merge branch '42-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 7fe3c8b -- Resolve "Migrate from tslint to eslint"
|/  
* af94615 -- Update project package.json version based on GitLab tag. Done by CI
*   105c759 -- Merge branch '41-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * eebd0de -- rename package scope and update dependencies
|/  
* 5f4642b -- Update project package.json version based on GitLab tag. Done by CI
*   6d4d5ea -- Merge branch '40-update-dependencies-to-new-format' into 'master'
|\  
| * a97e66c -- Resolve "Update dependencies to new format"
|/  
* 3fe51b8 -- Update project package.json version based on GitLab tag. Done by CI
*   670bd06 -- Merge branch '39-rename-package-to-kypo-topology-graph' into 'master'
|\  
| * f22bbe7 -- Resolve "Rename package to @kypo/topology-graph"
|/  
*   973f029 -- Merge branch '38-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * fa1b651 -- Remove personal info from README
|/  
*   aae734f -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 41bbf78 -- Resolve "Use Cypress image in CI"
|/  
* ae4f343 -- Update project package.json version based on GitLab tag. Done by CI
*   19a56f6 -- Merge branch '36-update-to-angualar-10' into 'master'
|\  
| * 4d6a53f -- Resolve "Update to Angular 10"
|/  
* bb653af -- Update project package.json version based on GitLab tag. Done by CI
### 12.1.2 Update to d3v7
* edf8c00 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7773b1c -- Merge branch '56-bump-version-of-d3' into 'master'
|\  
| * e5e7fb2 -- Resolve "Bump version of d3"
|/  
* ba84d32 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 52c67ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   e3a5ddf -- Merge branch '55-add-attributes-gui_access-and-os_type-to-routerdto' into 'master'
|\  
| * 57823ce -- Add missing attributes. Modified mapper accordingly.
|/  
* 37d0f58 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b31153f -- [CI/CD] Update packages.json version based on GitLab tag.
*   ecbd2f5 -- Merge branch '53-dispaly-open-cli-and-open-gui-option-based-on-the-os-type-and-guiaccess-attribute-of-the-host' into 'master'
|\  
| * bd8032c -- Resolve "Dispaly Open CLI and Open GUI option based on the OS type and guiAccess attribute of the host"
|/  
*   d683ace -- Merge branch '54-add-console-preload' into 'master'
|\  
| * 69439f1 -- Resolve "Add console preload"
|/  
*   b740a39 -- Merge branch '51-switching-the-host-console-access-to-a-single-phase' into 'master'
|\  
| * 3d4a8f1 -- Remove loading of spice console
|/  
* 9b52172 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 340fa12 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e42f178 -- Merge branch '50-add-simple-legend-for-topology-nodes' into 'master'
|\  
| * 344b252 -- Resolve "Add simple legend for topology nodes"
|/  
*   cf7e3b1 -- Merge branch '52-add-license-file' into 'master'
|\  
| * 2fd2ad6 -- Add license file
|/  
*   1e4a62f -- Merge branch '49-update-the-look-of-topology-nodes' into 'master'
|\  
| * 00fad21 -- Update nodes visibility and icons
|/  
* a9aa75b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 73354b2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   34da99c -- Merge branch '48-add-option-to-open-console-using-apache-guacamole' into 'master'
|\  
| * 916ef3a -- Resolve "Add option to open console using apache guacamole"
|/  
* b8b6b33 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 70fd3a9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8a59576 -- Merge branch '47-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * b6a4002 -- Update gitlab CI
|/  
* a291334 -- Update project package.json version based on GitLab tag. Done by CI
*   65ea149 -- Merge branch '46-update-to-angular-12' into 'master'
|\  
| * cbe2590 -- Resolve "Update to Angular 12"
|/  
* 9e6fcf5 -- Update project package.json version based on GitLab tag. Done by CI
*   4654845 -- Merge branch '45-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0aeb4a5 -- Update peerDependencies and dependencies
|/  
*   ab1fdb6 -- Merge branch '44-update-to-angular-11' into 'master'
|\  
| * 90fe8a8 -- Resolve "Update to Angular 11"
|/  
*   2f94029 -- Merge branch '43-recreate-package-lock-for-new-package-registry-muni-kypo-crp-frontend-angular-models-kypo' into 'master'
|\  
| * b8c9c07 -- reacreate package lock
|/  
*   6db319a -- Merge branch '42-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 7fe3c8b -- Resolve "Migrate from tslint to eslint"
|/  
* af94615 -- Update project package.json version based on GitLab tag. Done by CI
*   105c759 -- Merge branch '41-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * eebd0de -- rename package scope and update dependencies
|/  
* 5f4642b -- Update project package.json version based on GitLab tag. Done by CI
*   6d4d5ea -- Merge branch '40-update-dependencies-to-new-format' into 'master'
|\  
| * a97e66c -- Resolve "Update dependencies to new format"
|/  
* 3fe51b8 -- Update project package.json version based on GitLab tag. Done by CI
*   670bd06 -- Merge branch '39-rename-package-to-kypo-topology-graph' into 'master'
|\  
| * f22bbe7 -- Resolve "Rename package to @kypo/topology-graph"
|/  
*   973f029 -- Merge branch '38-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * fa1b651 -- Remove personal info from README
|/  
*   aae734f -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 41bbf78 -- Resolve "Use Cypress image in CI"
|/  
* ae4f343 -- Update project package.json version based on GitLab tag. Done by CI
*   19a56f6 -- Merge branch '36-update-to-angualar-10' into 'master'
|\  
| * 4d6a53f -- Resolve "Update to Angular 10"
|/  
* bb653af -- Update project package.json version based on GitLab tag. Done by CI
### 12.1.1 Mapper modification according to additional attributes.
* 52c67ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   e3a5ddf -- Merge branch '55-add-attributes-gui_access-and-os_type-to-routerdto' into 'master'
|\  
| * 57823ce -- Add missing attributes. Modified mapper accordingly.
|/  
* 37d0f58 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b31153f -- [CI/CD] Update packages.json version based on GitLab tag.
*   ecbd2f5 -- Merge branch '53-dispaly-open-cli-and-open-gui-option-based-on-the-os-type-and-guiaccess-attribute-of-the-host' into 'master'
|\  
| * bd8032c -- Resolve "Dispaly Open CLI and Open GUI option based on the OS type and guiAccess attribute of the host"
|/  
*   d683ace -- Merge branch '54-add-console-preload' into 'master'
|\  
| * 69439f1 -- Resolve "Add console preload"
|/  
*   b740a39 -- Merge branch '51-switching-the-host-console-access-to-a-single-phase' into 'master'
|\  
| * 3d4a8f1 -- Remove loading of spice console
|/  
* 9b52172 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 340fa12 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e42f178 -- Merge branch '50-add-simple-legend-for-topology-nodes' into 'master'
|\  
| * 344b252 -- Resolve "Add simple legend for topology nodes"
|/  
*   cf7e3b1 -- Merge branch '52-add-license-file' into 'master'
|\  
| * 2fd2ad6 -- Add license file
|/  
*   1e4a62f -- Merge branch '49-update-the-look-of-topology-nodes' into 'master'
|\  
| * 00fad21 -- Update nodes visibility and icons
|/  
* a9aa75b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 73354b2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   34da99c -- Merge branch '48-add-option-to-open-console-using-apache-guacamole' into 'master'
|\  
| * 916ef3a -- Resolve "Add option to open console using apache guacamole"
|/  
* b8b6b33 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 70fd3a9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8a59576 -- Merge branch '47-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * b6a4002 -- Update gitlab CI
|/  
* a291334 -- Update project package.json version based on GitLab tag. Done by CI
*   65ea149 -- Merge branch '46-update-to-angular-12' into 'master'
|\  
| * cbe2590 -- Resolve "Update to Angular 12"
|/  
* 9e6fcf5 -- Update project package.json version based on GitLab tag. Done by CI
*   4654845 -- Merge branch '45-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0aeb4a5 -- Update peerDependencies and dependencies
|/  
*   ab1fdb6 -- Merge branch '44-update-to-angular-11' into 'master'
|\  
| * 90fe8a8 -- Resolve "Update to Angular 11"
|/  
*   2f94029 -- Merge branch '43-recreate-package-lock-for-new-package-registry-muni-kypo-crp-frontend-angular-models-kypo' into 'master'
|\  
| * b8c9c07 -- reacreate package lock
|/  
*   6db319a -- Merge branch '42-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 7fe3c8b -- Resolve "Migrate from tslint to eslint"
|/  
* af94615 -- Update project package.json version based on GitLab tag. Done by CI
*   105c759 -- Merge branch '41-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * eebd0de -- rename package scope and update dependencies
|/  
* 5f4642b -- Update project package.json version based on GitLab tag. Done by CI
*   6d4d5ea -- Merge branch '40-update-dependencies-to-new-format' into 'master'
|\  
| * a97e66c -- Resolve "Update dependencies to new format"
|/  
* 3fe51b8 -- Update project package.json version based on GitLab tag. Done by CI
*   670bd06 -- Merge branch '39-rename-package-to-kypo-topology-graph' into 'master'
|\  
| * f22bbe7 -- Resolve "Rename package to @kypo/topology-graph"
|/  
*   973f029 -- Merge branch '38-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * fa1b651 -- Remove personal info from README
|/  
*   aae734f -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 41bbf78 -- Resolve "Use Cypress image in CI"
|/  
* ae4f343 -- Update project package.json version based on GitLab tag. Done by CI
*   19a56f6 -- Merge branch '36-update-to-angualar-10' into 'master'
|\  
| * 4d6a53f -- Resolve "Update to Angular 10"
|/  
* bb653af -- Update project package.json version based on GitLab tag. Done by CI
### 12.1.0 Removed generation of Spice console. Added preload of Spice consoles for all VMs in topology. Open CLI and open GUI added based on os type and gui access of host.
* b31153f -- [CI/CD] Update packages.json version based on GitLab tag.
*   ecbd2f5 -- Merge branch '53-dispaly-open-cli-and-open-gui-option-based-on-the-os-type-and-guiaccess-attribute-of-the-host' into 'master'
|\  
| * bd8032c -- Resolve "Dispaly Open CLI and Open GUI option based on the OS type and guiAccess attribute of the host"
|/  
*   d683ace -- Merge branch '54-add-console-preload' into 'master'
|\  
| * 69439f1 -- Resolve "Add console preload"
|/  
*   b740a39 -- Merge branch '51-switching-the-host-console-access-to-a-single-phase' into 'master'
|\  
| * 3d4a8f1 -- Remove loading of spice console
|/  
* 9b52172 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 340fa12 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e42f178 -- Merge branch '50-add-simple-legend-for-topology-nodes' into 'master'
|\  
| * 344b252 -- Resolve "Add simple legend for topology nodes"
|/  
*   cf7e3b1 -- Merge branch '52-add-license-file' into 'master'
|\  
| * 2fd2ad6 -- Add license file
|/  
*   1e4a62f -- Merge branch '49-update-the-look-of-topology-nodes' into 'master'
|\  
| * 00fad21 -- Update nodes visibility and icons
|/  
* a9aa75b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 73354b2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   34da99c -- Merge branch '48-add-option-to-open-console-using-apache-guacamole' into 'master'
|\  
| * 916ef3a -- Resolve "Add option to open console using apache guacamole"
|/  
* b8b6b33 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 70fd3a9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8a59576 -- Merge branch '47-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * b6a4002 -- Update gitlab CI
|/  
* a291334 -- Update project package.json version based on GitLab tag. Done by CI
*   65ea149 -- Merge branch '46-update-to-angular-12' into 'master'
|\  
| * cbe2590 -- Resolve "Update to Angular 12"
|/  
* 9e6fcf5 -- Update project package.json version based on GitLab tag. Done by CI
*   4654845 -- Merge branch '45-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0aeb4a5 -- Update peerDependencies and dependencies
|/  
*   ab1fdb6 -- Merge branch '44-update-to-angular-11' into 'master'
|\  
| * 90fe8a8 -- Resolve "Update to Angular 11"
|/  
*   2f94029 -- Merge branch '43-recreate-package-lock-for-new-package-registry-muni-kypo-crp-frontend-angular-models-kypo' into 'master'
|\  
| * b8c9c07 -- reacreate package lock
|/  
*   6db319a -- Merge branch '42-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 7fe3c8b -- Resolve "Migrate from tslint to eslint"
|/  
* af94615 -- Update project package.json version based on GitLab tag. Done by CI
*   105c759 -- Merge branch '41-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * eebd0de -- rename package scope and update dependencies
|/  
* 5f4642b -- Update project package.json version based on GitLab tag. Done by CI
*   6d4d5ea -- Merge branch '40-update-dependencies-to-new-format' into 'master'
|\  
| * a97e66c -- Resolve "Update dependencies to new format"
|/  
* 3fe51b8 -- Update project package.json version based on GitLab tag. Done by CI
*   670bd06 -- Merge branch '39-rename-package-to-kypo-topology-graph' into 'master'
|\  
| * f22bbe7 -- Resolve "Rename package to @kypo/topology-graph"
|/  
*   973f029 -- Merge branch '38-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * fa1b651 -- Remove personal info from README
|/  
*   aae734f -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 41bbf78 -- Resolve "Use Cypress image in CI"
|/  
* ae4f343 -- Update project package.json version based on GitLab tag. Done by CI
*   19a56f6 -- Merge branch '36-update-to-angualar-10' into 'master'
|\  
| * 4d6a53f -- Resolve "Update to Angular 10"
|/  
* bb653af -- Update project package.json version based on GitLab tag. Done by CI
### 12.0.3 Update topology appearance with new special nodes, add legend
* 340fa12 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e42f178 -- Merge branch '50-add-simple-legend-for-topology-nodes' into 'master'
|\  
| * 344b252 -- Resolve "Add simple legend for topology nodes"
|/  
*   cf7e3b1 -- Merge branch '52-add-license-file' into 'master'
|\  
| * 2fd2ad6 -- Add license file
|/  
*   1e4a62f -- Merge branch '49-update-the-look-of-topology-nodes' into 'master'
|\  
| * 00fad21 -- Update nodes visibility and icons
|/  
* a9aa75b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 73354b2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   34da99c -- Merge branch '48-add-option-to-open-console-using-apache-guacamole' into 'master'
|\  
| * 916ef3a -- Resolve "Add option to open console using apache guacamole"
|/  
* b8b6b33 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 70fd3a9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8a59576 -- Merge branch '47-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * b6a4002 -- Update gitlab CI
|/  
* a291334 -- Update project package.json version based on GitLab tag. Done by CI
*   65ea149 -- Merge branch '46-update-to-angular-12' into 'master'
|\  
| * cbe2590 -- Resolve "Update to Angular 12"
|/  
* 9e6fcf5 -- Update project package.json version based on GitLab tag. Done by CI
*   4654845 -- Merge branch '45-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0aeb4a5 -- Update peerDependencies and dependencies
|/  
*   ab1fdb6 -- Merge branch '44-update-to-angular-11' into 'master'
|\  
| * 90fe8a8 -- Resolve "Update to Angular 11"
|/  
*   2f94029 -- Merge branch '43-recreate-package-lock-for-new-package-registry-muni-kypo-crp-frontend-angular-models-kypo' into 'master'
|\  
| * b8c9c07 -- reacreate package lock
|/  
*   6db319a -- Merge branch '42-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 7fe3c8b -- Resolve "Migrate from tslint to eslint"
|/  
* af94615 -- Update project package.json version based on GitLab tag. Done by CI
*   105c759 -- Merge branch '41-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * eebd0de -- rename package scope and update dependencies
|/  
* 5f4642b -- Update project package.json version based on GitLab tag. Done by CI
*   6d4d5ea -- Merge branch '40-update-dependencies-to-new-format' into 'master'
|\  
| * a97e66c -- Resolve "Update dependencies to new format"
|/  
* 3fe51b8 -- Update project package.json version based on GitLab tag. Done by CI
*   670bd06 -- Merge branch '39-rename-package-to-kypo-topology-graph' into 'master'
|\  
| * f22bbe7 -- Resolve "Rename package to @kypo/topology-graph"
|/  
*   973f029 -- Merge branch '38-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * fa1b651 -- Remove personal info from README
|/  
*   aae734f -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 41bbf78 -- Resolve "Use Cypress image in CI"
|/  
* ae4f343 -- Update project package.json version based on GitLab tag. Done by CI
*   19a56f6 -- Merge branch '36-update-to-angualar-10' into 'master'
|\  
| * 4d6a53f -- Resolve "Update to Angular 10"
|/  
* bb653af -- Update project package.json version based on GitLab tag. Done by CI
### 12.0.2 Added option to open VM CLI or GUI using Apache Guacamole.
* 73354b2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   34da99c -- Merge branch '48-add-option-to-open-console-using-apache-guacamole' into 'master'
|\  
| * 916ef3a -- Resolve "Add option to open console using apache guacamole"
|/  
* b8b6b33 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 70fd3a9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8a59576 -- Merge branch '47-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * b6a4002 -- Update gitlab CI
|/  
* a291334 -- Update project package.json version based on GitLab tag. Done by CI
*   65ea149 -- Merge branch '46-update-to-angular-12' into 'master'
|\  
| * cbe2590 -- Resolve "Update to Angular 12"
|/  
* 9e6fcf5 -- Update project package.json version based on GitLab tag. Done by CI
*   4654845 -- Merge branch '45-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0aeb4a5 -- Update peerDependencies and dependencies
|/  
*   ab1fdb6 -- Merge branch '44-update-to-angular-11' into 'master'
|\  
| * 90fe8a8 -- Resolve "Update to Angular 11"
|/  
*   2f94029 -- Merge branch '43-recreate-package-lock-for-new-package-registry-muni-kypo-crp-frontend-angular-models-kypo' into 'master'
|\  
| * b8c9c07 -- reacreate package lock
|/  
*   6db319a -- Merge branch '42-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 7fe3c8b -- Resolve "Migrate from tslint to eslint"
|/  
* af94615 -- Update project package.json version based on GitLab tag. Done by CI
*   105c759 -- Merge branch '41-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * eebd0de -- rename package scope and update dependencies
|/  
* 5f4642b -- Update project package.json version based on GitLab tag. Done by CI
*   6d4d5ea -- Merge branch '40-update-dependencies-to-new-format' into 'master'
|\  
| * a97e66c -- Resolve "Update dependencies to new format"
|/  
* 3fe51b8 -- Update project package.json version based on GitLab tag. Done by CI
*   670bd06 -- Merge branch '39-rename-package-to-kypo-topology-graph' into 'master'
|\  
| * f22bbe7 -- Resolve "Rename package to @kypo/topology-graph"
|/  
*   973f029 -- Merge branch '38-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * fa1b651 -- Remove personal info from README
|/  
*   aae734f -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 41bbf78 -- Resolve "Use Cypress image in CI"
|/  
* ae4f343 -- Update project package.json version based on GitLab tag. Done by CI
*   19a56f6 -- Merge branch '36-update-to-angualar-10' into 'master'
|\  
| * 4d6a53f -- Resolve "Update to Angular 10"
|/  
* bb653af -- Update project package.json version based on GitLab tag. Done by CI
### 12.0.1 Update gitlab CI
* 70fd3a9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8a59576 -- Merge branch '47-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * b6a4002 -- Update gitlab CI
|/  
* a291334 -- Update project package.json version based on GitLab tag. Done by CI
*   65ea149 -- Merge branch '46-update-to-angular-12' into 'master'
|\  
| * cbe2590 -- Resolve "Update to Angular 12"
|/  
* 9e6fcf5 -- Update project package.json version based on GitLab tag. Done by CI
*   4654845 -- Merge branch '45-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0aeb4a5 -- Update peerDependencies and dependencies
|/  
*   ab1fdb6 -- Merge branch '44-update-to-angular-11' into 'master'
|\  
| * 90fe8a8 -- Resolve "Update to Angular 11"
|/  
*   2f94029 -- Merge branch '43-recreate-package-lock-for-new-package-registry-muni-kypo-crp-frontend-angular-models-kypo' into 'master'
|\  
| * b8c9c07 -- reacreate package lock
|/  
*   6db319a -- Merge branch '42-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 7fe3c8b -- Resolve "Migrate from tslint to eslint"
|/  
* af94615 -- Update project package.json version based on GitLab tag. Done by CI
*   105c759 -- Merge branch '41-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * eebd0de -- rename package scope and update dependencies
|/  
* 5f4642b -- Update project package.json version based on GitLab tag. Done by CI
*   6d4d5ea -- Merge branch '40-update-dependencies-to-new-format' into 'master'
|\  
| * a97e66c -- Resolve "Update dependencies to new format"
|/  
* 3fe51b8 -- Update project package.json version based on GitLab tag. Done by CI
*   670bd06 -- Merge branch '39-rename-package-to-kypo-topology-graph' into 'master'
|\  
| * f22bbe7 -- Resolve "Rename package to @kypo/topology-graph"
|/  
*   973f029 -- Merge branch '38-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * fa1b651 -- Remove personal info from README
|/  
*   aae734f -- Merge branch '37-use-cypress-image-in-ci' into 'master'
|\  
| * 41bbf78 -- Resolve "Use Cypress image in CI"
|/  
* ae4f343 -- Update project package.json version based on GitLab tag. Done by CI
*   19a56f6 -- Merge branch '36-update-to-angualar-10' into 'master'
|\  
| * 4d6a53f -- Resolve "Update to Angular 10"
|/  
* bb653af -- Update project package.json version based on GitLab tag. Done by CI
